#ifndef DAS_LUMIUNC
#define DAS_LUMIUNC
#include <filesystem>
#include <string>
#include <vector>

#include <exceptions.h>

namespace DAS {

class RecEvent;

namespace Normalisation {

struct LumiUnc {

    std::vector<float> factors; //!< all factors
    std::vector<std::string> sources; //!< name of the sources of uncertainty

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor. Parses the input file in INFO format, expecting a description
    /// of correlated and uncorrelated uncertainties. Example:
    //  ```
    /// uncorrelated
    /// {
    ///     2017 2.0
    ///     2018 1.5
    /// }
    /// correlated
    /// {
    ///     sourceA
    ///     {
    ///         2017 0.3
    ///         2018 0.2
    ///     }
    ///     ; there may be as many of these blocks as one wants
    /// }
    /// ```
    /// NOTE: it is expected that the same years be given in all blocks in the same order
    LumiUnc (const std::filesystem::path&, //!< path to info file
             int); //!< (4-digit) year

    ////////////////////////////////////////////////////////////////////////////////
    /// Extends the vector of weights
    void operator() (RecEvent *) const;
};

} // end of Normalisation namespace

} // end of DAS namespace
#endif
