#include <cassert>
#include <iostream>
#include <limits>
#include <cstdlib>
#include <vector>
#include <map>
#include <experimental/filesystem>

#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"
#include "Core/CommonTools/interface/terminal.h"
#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/ControlPlots.h"

#include <TROOT.h>
#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH2.h>

#include "Math/VectorUtil.h"
#include "TriggerEff.h"

#include "Core/Prefiring/interface/applyPrefiringWeights.h"

using namespace std;
using namespace experimental::filesystem;
using namespace DAS;

////////////////////////////////////////////////////////////////////////////////
///// Application of prefiring weights in data or simulation.
/////
///// Calls a functor


void applyPrefNormZeroBias 
             (const fs::path& input,  //!< name of input root file 
              const fs::path& output, //!< name of output root file
              PrefOpt prefOpt, //!< option to choose the right map(s)
              auto total_lumi,
              auto maxpt,
              int nSplit = 1, //!< number of jobs/tasks/cores
              int nNow = 0)   //!< index of job/task/core
{
    // Get old file, old tree and set top branch address
    assert(fs::exists(input));
    TChain * oldchain = new TChain("events"); // arg = path inside the ntuple
    oldchain->Add(input.c_str());

    Event * evnt = nullptr;
    oldchain->SetBranchAddress("event", &evnt);
    vector<RecJet> * recJets = nullptr;
    oldchain->SetBranchAddress("recJets", &recJets);
    Trigger * trigger = nullptr;
    oldchain->SetBranchAddress("trigger", &trigger);

    if (fs::exists(output))
        cerr << red << output << " will be overwritten\n" << normal;
    TFile * newfile = TFile::Open(output.c_str(), "RECREATE");
    TTree * newtree = oldchain->CloneTree(0);

    MetaInfo metainfo(newtree);
    if (nNow == 0) metainfo.Print();
    metainfo.AddCorrection("PrefiringWeights");
    metainfo.AddCorrection("normalised");
    metainfo.AddRecEvWgt("Prefup");
    metainfo.AddRecEvWgt("Prefdown");
    bool isMC = metainfo.isMC();

    vector<GenJet> * genJets = nullptr;
    if (isMC) oldchain->SetBranchAddress("genJets", &genJets);

    int year = metainfo.year();
    assert(year > 2015 && year < 2019);
    
    //Define prefiring weight functor
    applyPrefiringWeightsFunctor apply(year, prefOpt, isMC);

    //Define control plots for prefiring weights
    ControlPlots::isMC = isMC;
    ControlPlots raw("raw");
    vector<ControlPlots> calib { ControlPlots("nominal_befNorm"),
                                 ControlPlots("upper_befNorm"  ),
                                 ControlPlots("lower_befNorm"  ) };
    auto totRecWgt = [&](size_t i) {
        return (isMC ? evnt->genWgts.front() : 1) * evnt->recWgts.at(i);
    };

    assert(total_lumi > 0);
    auto inv_total_lumi = 1./total_lumi;

    newfile->cd();

    // Define control plots for normalisation
     ControlPlots::isMC = false;

    vector<ControlPlots> corr ;
    for (int i=0; i<metainfo.GetNRecEvWgts(); i++)
        corr.push_back(ControlPlots(metainfo.GetRecEvWgt(i)));

    cout << "looping over events:" << endl;

    Looper looper(__func__, oldchain, nSplit, nNow);
    while (looper.Next()) {
    // apply prefiring weights
    if (isMC) raw(*genJets, evnt->genWgts.front());
        raw(*recJets, totRecWgt(0));
        apply(*evnt, *recJets);
        for (size_t i = 0; i < calib.size(); ++i) {
            if (isMC) calib.at(i)(*genJets, evnt->genWgts.front());
            if(recJets->size()==0) continue;
            calib.at(i)(*recJets, totRecWgt(i));
        }

        //apply normalisation
        // // we find the leading jet in tracker acceptance
        auto leadingInTk = recJets->begin();
        while (leadingInTk != recJets->end()
                && abs(leadingInTk->Rapidity()) >= 2.5)
            ++leadingInTk;
        // removed events alreadu covered by jet triggers
        if (leadingInTk != recJets->end() && leadingInTk->CorrPt() >= maxpt) continue;
        // setting the inverse of the eff lumi to the event including correction of for the trigger efficiency
        // corrections applied to all the prefiring variations
       for (size_t i = 0; i < corr.size(); ++i)
            evnt->recWgts.at(i) *= inv_total_lumi;

        newtree->Fill();
        for(size_t i=0; i<corr.size(); i++){
            if(recJets->size()==0) continue;
            corr.at(i)(*recJets, evnt->recWgts.at(i));
        }
    }

    cout << "saving" << endl;
    newtree->AutoSave();
    raw.Write(newfile);
    for (size_t i = 0; i < calib.size(); ++i)
        calib.at(i).Write(newfile);
    TDirectory * controlplotsPref = newfile->mkdir("Prefiring");
    apply.Write(controlplotsPref);
    for (size_t i = 0; i < corr.size(); ++i)
        corr.at(i).Write(newfile);
    auto controlplots = newfile->mkdir("controlplots");
    controlplots->cd();
    newfile->Close();
    delete oldchain;
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 4) {
        cout << argv[0] << " input output option [nSplit [nNow]]\n"
             << "\twhere\tinput = n-tuple\n"
             << "\t     \toutput = modified n-tuple\n"
             << "\t     \toption = `AverageMap` (default; only possible option for MC), `MapsPerEra`, or `SmoothMapsPerEra`\n"
             << flush;
        return EXIT_SUCCESS;
    }

    fs::path input = argv[1],
             output = argv[2];

    TString option = argv[3];
    PrefOpt prefOpt = option =="MapsPerEra"       ?   PrefOpt::kMapsPerEra       :
                      option =="SmoothMapsPerEra" ?   PrefOpt::kSmoothMapsPerEra :
                    /*option =="AverageMap"       ?*/ PrefOpt::kAverageMap       ;

    auto total_lumi = atof(argv[4]),
         maxpt = atof(argv[5]);
    int nNow = 0, nSplit = 1;
    if (argc > 6) nSplit = atoi(argv[6]);
    if (argc > 7) nNow = atoi(argv[7]);

    applyPrefNormZeroBias(input, output, prefOpt, total_lumi, maxpt, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
 
