#include <cstdlib>
#include <thread>
#include <iostream>
#include <filesystem>
#include <utility>
#include <optional>

#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>
#include <TH1D.h>
#include <TRegexp.h>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"

#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/ControlPlots.h"

#include <darwin.h>

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Normalisation {

////////////////////////////////////////////////////////////////////////////////
/// Retrieve the sum of the weights obtained with `getSumWeights` or
/// with `mergeNtuples`. The weights is expected to be found in a trivial
/// histogram, with only one bin.
float getSumOfWeights
    (const vector<fs::path>& inputs) //!< files containing the histogram
{
    unique_ptr<TH1> h = DT::GetHist<TH1>(inputs, "hSumWgt");
    auto sumWgts = h->GetBinContent(1);
    if (sumWgts <= 0)
        BOOST_THROW_EXCEPTION( DE::BadInput("Null sum of weights.", h) );
    return sumWgts;
}

////////////////////////////////////////////////////////////////////////////////
/// Normalise with sum of weights (to be computed) and cross section (given)
///
/// *Note*: a cut-off for events with hard scale above 5 TeV is applied, since these events are not realistic
void applyMClumi
        (const vector<fs::path>& inputs, //!< input ROOT files (n-tuple)
         const fs::path& output, //!< output ROOT file (n-tuple)
         const pt::ptree& config, //!< config handled with `Darwin::Tools::Options`
         const int steering, //!< parameters obtained from explicit options
         const DT::Slice slice = {1,0} //!< number and index of slice
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;
    
    unique_ptr<TChain> tIn = DT::GetChain(inputs);
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = unique_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    if (!isMC) BOOST_THROW_EXCEPTION( DE::BadInput("Only MC may be used as input.",
                                      make_unique<TFile>(inputs.front().c_str() )) );

    // normalisation factor
    auto sumw = Normalisation::getSumOfWeights(inputs); // TODO: not necessarily from the same input files... (e.g. if using `getSumWgts`)
    if (sumw <= 0)
        BOOST_THROW_EXCEPTION( DE::BadInput("Null sum of weights.",
                               make_unique<TFile>(inputs.front().c_str() )) ); // TODO?
    float xsection = config.get<float>("corrections.xsection");
    if (xsection <= 0)
        BOOST_THROW_EXCEPTION( invalid_argument("Negative cross section.") );
    auto factor = xsection/sumw;
    cout << xsection << '\t' << sumw << '\t' << factor << endl;
    metainfo.Set<float>("corrections", "xsection", xsection);

    // declaring branches
    RecEvent * recEvt = nullptr;
    GenEvent * genEvt = nullptr;
    PileUp * pileup = nullptr;
    tIn->SetBranchAddress("recEvent", &recEvt);
    if (isMC)
        tIn->SetBranchAddress("genEvent", &genEvt);
    tIn->SetBranchAddress("pileup", &pileup);
    vector<GenJet> * genjets = nullptr;
    vector<RecJet> * recjets = nullptr;
    if (branchExists(tIn, "recJets"))
        tIn->SetBranchAddress("recJets", &recjets);
    if (branchExists(tIn, "genJets"))
        tIn->SetBranchAddress("genJets", &genjets);

    // control plot
    ControlPlots::isMC = true;
    ControlPlots plots("controlplots");

    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        // cut-off for 5-TeV events
        if (genEvt->hard_scale > 5000 /* GeV */) continue; // TODO?

        // sanity check
        if (genEvt->weights.size() != 1 || recEvt->weights.size() != 1)
            BOOST_THROW_EXCEPTION( DE::AnomalousEvent("Unexpected event weights", tIn) );
        if (recjets != nullptr && recjets->size() > 0 && recjets->front().scales.size() > 1)
            BOOST_THROW_EXCEPTION( DE::AnomalousEvent("Unexpected jet energy scale variations", tIn) );

        // renormalisation
        genEvt->weights.front() *= factor;
        if ((steering & DT::fill) == DT::fill) tOut->Fill();

        // control plot
        if (genjets != nullptr)
            plots(*genjets, genEvt->weights.front()                          );
        if (recjets != nullptr)
            plots(*recjets, genEvt->weights.front() * recEvt->weights.front());
    }

    metainfo.Set<bool>("git", "complete", true);
    fOut->cd();
    tOut->Write();

    plots.Write(fOut.get());

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of DAS::Normalisation namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Normalise the MC samples to the cross section.",
                            DT::config | DT::split | DT::fill);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<float>("xsection", "corrections.xsection", "cross section value");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::Normalisation::applyMClumi(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
