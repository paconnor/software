#ifndef DOXYGEN_SHOULD_SKIP_THIS
#define DOXYGEN_SHOULD_SKIP_THIS
#define BOOST_TEST_MODULE testToy
#include <boost/test/included/unit_test.hpp>
#include "Core/Uncertainties/bin/getToyCalculation.cc"
#include "Core/Unfolding/interface/Observable.h"

#include <darwin.h>

#include <filesystem>
#include <vector>

#include <TH1.h>
#include <TH2.h>

using namespace std;
namespace fs = filesystem;
using namespace Eigen;
namespace utf = boost::unit_test;

class TestTransformer final : public DAS::Unfolding::Transformer {

public:

    TestTransformer (TUnfoldBinning * pre) : Transformer(pre, false)
    {
        postBinning->AddAxis("second_axis", 5, 4.5, 9.5, false, false);
    }

    ~TestTransformer () = default;

    void Transform (const Eigen::VectorXd& x) const override
    {
        y(4) = 0;
        y(5) = 0;
        y(6) = x(3) + x(5);
        y(7) = x(4) + x(6);
        y(8) = 0;
    }
};


BOOST_AUTO_TEST_SUITE( teddy, * utf::tolerance(0.01) )

    BOOST_AUTO_TEST_CASE( single_distribution )
    {
        DT::StandardInit();

        auto   hIn = make_unique<TH1D>(  "hIn",    "hIn", 3, 0.5, 3.5);
        auto covIn = make_unique<TH2D>("covIn",  "covIn", 3, 0.5, 3.5,
                                                          3, 0.5, 3.5);

        cout << "\nInput\n" << endl;
        hIn->SetBinContent(1, 1);
        hIn->SetBinContent(2, 2);
        hIn->SetBinContent(3, 1);
        hIn->Print("all");

        covIn->SetBinContent(1, 1, 1);
        covIn->SetBinContent(1, 2, 1);
        covIn->SetBinContent(1, 3, 0);
        covIn->SetBinContent(2, 1, 1);
        covIn->SetBinContent(2, 2, 4);
        covIn->SetBinContent(2, 3, 0);
        covIn->SetBinContent(3, 1, 0);
        covIn->SetBinContent(3, 2, 0);
        covIn->SetBinContent(3, 3, 1);
        covIn->Print("all");

        auto func = [](const VectorXd& x) {
            VectorXd y(2);
            y(0) = x(0) + x(1);
            y(1) = x(2);
            return y;
        };

        using namespace DAS;
        Teddy toy(hIn, covIn, func);
        for (int i = 0; i < 1e6; ++i)
            toy.play();
        auto [x,cov] = toy.buy();

        cout << "x:\n" << x << "\n\n"
             << "cov:\n" << cov << endl;

        BOOST_TEST( x(0) == 3 );
        BOOST_TEST( x(1) == 1 );
        
        BOOST_TEST( cov(0,0) == 7);
        BOOST_TEST( cov(0,1) == 0);
        BOOST_TEST( cov(1,0) == 0);
        BOOST_TEST( cov(1,1) == 1);
    }

    BOOST_AUTO_TEST_CASE( double_distribution )
    {
        // pre distribution
        auto preBng = new TUnfoldBinning("pre");

        auto firstPreBng = new TUnfoldBinning("first_dist");
        firstPreBng->AddAxis("first_axis", 4, 0.5, 4.5, false, false);
        preBng->AddBinning(firstPreBng);

        auto secondPreBng = new TUnfoldBinning("second_dist");
        secondPreBng->AddAxis("second_axis", 4, 4.5, 8.5, false, false);
        preBng->AddBinning(secondPreBng);

        auto hIn = unique_ptr<TH1D>((TH1D*)preBng->CreateHistogram("hIn", false, nullptr, "input"));
        auto covIn = unique_ptr<TH2D>(preBng->CreateErrorMatrixHistogram("covIn", false, nullptr, "input"));

        hIn->SetBinContent(1, 1);    hIn->SetBinContent(5, 3);
        hIn->SetBinContent(2, 2);    hIn->SetBinContent(6, 4);
        hIn->SetBinContent(3, 5);    hIn->SetBinContent(7, 7);
        hIn->SetBinContent(4, 0);    hIn->SetBinContent(8, 10);

        for (int i = 1; i <= 8; ++i)
            covIn->SetBinContent(i,i,hIn->GetBinContent(i));

        ////////////////////////////////////////////////////

        using namespace DAS;
        using namespace Unfolding;

        // post distribution
        auto postBng = new TUnfoldBinning("post");
        vector<unique_ptr<Transformer>> transformers;
        transformers.emplace_back(make_unique<Transformer>(firstPreBng));
        transformers.emplace_back(make_unique<TestTransformer>(secondPreBng));
        postBng->AddBinning(transformers[0]->postBinning);
        postBng->AddBinning(transformers[1]->postBinning);

        function<VectorXd(const VectorXd &)> transformation =
            [&transformers](const VectorXd& x) {
                Transformer::y = VectorXd::Zero(9);
                for (const auto& t: transformers)
                    t->Transform(x);
                return Transformer::y;
            };

        for (auto& t: transformers)
            t->RemoveBadInputBins(hIn.get(), covIn.get());

        ////////////////////////////////////////////////////

        Teddy toy(hIn, covIn, transformation);
        for (int i = 0; i < 1e6; ++i)
            toy.play();
        auto [x,cov] = toy.buy();

        cout << "x:\n" << x << "\n\n"
             << "cov:\n" << cov << endl;

        VectorXd xTruth(9);
        xTruth << 1, 2, 5, 0, 0, 0, 4, 10, 0;
        for (int i = 0; i < 9; ++i) {
            cout << i << ' ' << xTruth(i) << ' ' << x(i) << endl;
            BOOST_TEST( x(i) == xTruth(i)  );
        }
    }

BOOST_AUTO_TEST_SUITE_END()

#endif
