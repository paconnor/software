#pragma once

#include <functional>
#include <algorithm>
#include <memory>

#include <Eigen/Dense>

#include <TRandom3.h>
#include <TH2.h>

class TUnfoldBinning;

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// identity (Eigen)
///
/// Trivial function returning argument, used as default function in the constructor of Teddy
Eigen::VectorXd identity (const Eigen::VectorXd& x);

////////////////////////////////////////////////////////////////////////////////
/// class Teddy
///
/// Use MC techniques to apply a function \f$f\f$ to a distribution $x$ with (non-diagonal) covariance matrix \f$\mathbf{V}\f$. 
/// 
/// Procedure:
///  1. Diagonalise the covariance matrix
///  2. Generate a multi-dimensional Gaussian event \f$\delta\f$ distributed according to the (sqrt of the) eigenvalues \f$\epsilon_i\f$
/// \f[
///     \delta_i \sim \mathcal{N}(0,\sqrt{\epsilon_i})
/// \f]
///  3. Transform back to the original base (where the cov matrix is non-diagonal)
///  4. Add rotated vector to the input distribution an apply the function \f$f\f$:
/// \f[
/// \mathbf{y}_k = 
/// \f]
///  5. Go back to 2. until a satisfying statistics is reached, with `Teddy::play()` (the criterion of satisfaction and the loop are to be defined outside of the `Teddy` instance)
///  6. Get output histogram and covariance matrix with `Teddy::buy()`
class Teddy {

    static Eigen::VectorXd H2V (const std::unique_ptr<TH1D>&); //!< conversion from (1D) histogram to vector
    static Eigen::MatrixXd H2M (const std::unique_ptr<TH2D>&); //!< conversion from (2D) histogram to matrix

    ////////////////////////////////////////////////////////////////////////////////
    /// Guess size of the output distribution by calling once the function with dummy input
    static int GuessOutDim (int, std::function<Eigen::VectorXd(const Eigen::VectorXd&)>);

    ////////////////////////////////////////////////////////////////////////////////
    /// Check the number of negative eigenvalues and fail if any is found
    static void SanityCheck (const Eigen::VectorXd&, Eigen::VectorXd&, double = 0);

    const Eigen::VectorXd vec; //!< distribution
    const Eigen::MatrixXd cov; //!< covariance matrix

    const int nOldBins, //!< number of bins of input distributions
              nNewBins; //!< number of bins of output distributions
    long long N; //!< number of calls

    Eigen::VectorXd eigVals; //!< eigenvalues of input covariance matrix
    Eigen::MatrixXd rotation; //!< rotation from diagonal basis to original basis

    Eigen::VectorXd sigma; //!< Gaussian widths in diagonal base

    Eigen::VectorXd sum; //!< sum of events, increased at each call and from which output distribution is estimated
    Eigen::MatrixXd sum2; //!< sum of tensor products of each event, increased at each call and from which output covariance is estimated

    std::function<Eigen::VectorXd(const Eigen::VectorXd &)> func; //!< transformation defining output as a function of the input

    TRandom3 random; //!< random generator

public:

    static bool verbose;

    ////////////////////////////////////////////////////////////////////////////////
    /// The rotation and the eigenvalues are determined from the input covariance matrix.
    /// Default function is the identity (useful for testing).
    Teddy
        (const Eigen::VectorXd&, //!< input distribution
         const Eigen::MatrixXd&, //!< input covariance matrix
         std::function<Eigen::VectorXd(const Eigen::VectorXd &)> = identity, //!< function to apply to the input distribution
         int = 42 //!< seed for random generator
         );

    ////////////////////////////////////////////////////////////////////////////////
    /// The rotation and the eigenvalues are determined from the input covariance matrix.
    /// Default function is the identity (useful for testing).
    Teddy
        (const std::unique_ptr<TH1D>&, //!< input distribution
         const std::unique_ptr<TH2D>&, //!< input covariance matrix
         std::function<Eigen::VectorXd(const Eigen::VectorXd &)> = identity, //!< function to apply to the input distribution
         int = 42 //!< seed for random generator
         );

    void play (); //!< Needs to be called from a loop, but requires no argument
    std::pair<Eigen::VectorXd, Eigen::MatrixXd> buy () const; //!< Get output distributions, can be called several times.

    ////////////////////////////////////////////////////////////////////////////////
    /// Get output distributions, can be called several times.
    /// Input histrogams are defined from scratch and filled.
    /// The statistical uncertainty of the 1D histogram correspondings to the square-root of the diagonal elements of the covariance matrix 
    template<typename... Args>
        std::pair<std::unique_ptr<TH1D>,std::unique_ptr<TH2D>> buy 
            (TString name, Args... args) const
    {
        using namespace std;
        using namespace Eigen;

        TAxis * axis = new TAxis(args...);
        int ndiv = axis->GetNbins();
        double * edges = new double[ndiv];
        axis->GetLowEdge(edges);
        edges[ndiv-1] = axis->GetBinUpEdge(ndiv);
        auto h   = make_unique<TH1D>(name         , name, ndiv-1, edges);
        auto cov = make_unique<TH2D>(name + "_cov", name, ndiv-1, edges, ndiv-1, edges);

        VectorXd x(nNewBins);
        MatrixXd err(nNewBins, nNewBins);
        tie(x,err) = buy();
    
        for (int i=0; i<nNewBins; ++i) {
            double content = x(i),
                   error = sqrt(err(i,i));
    
            h->SetBinContent(i+1, content);
            h->SetBinError  (i+1, error  );
    
            for (int j=0; j<nNewBins; ++j)
                cov->SetBinContent(i+1,j+1, err(i,j));
        }

        return make_pair<unique_ptr<TH1D>,unique_ptr<TH2D>>(move(h),move(cov));
    }
};
bool Teddy::verbose = true;

} // end of DAS namespace
