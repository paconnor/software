# Muon calibration

[General TWiki](https://twiki.cern.ch/twiki/bin/view/CMS/SWGuideMuonIdRun2) [GitLab](https://gitlab.cern.ch/cms-muonPOG)

## Energy scale (Rochester)

[Tables](https://gitlab.cern.ch/akhukhun/roccor)

## Efficiency

For reconstruction, identification, and isolation.

[TWiki](https://twiki.cern.ch/twiki/bin/viewauth/CMS/MuonReferenceEffsRun2) [Tables](https://gitlab.cern.ch/cms-muonPOG/muonefficiencies)
