#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <filesystem>

#include "Core/Objects/interface/Lepton.h"
#include "Core/Objects/interface/Photon.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"

#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TH1.h>
#include <TH3.h>

#include "Math/VectorUtil.h"

#include <darwin.h>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Muon {

vector<float> GetMassEdges (int nbins = 100, double M = 3000, double m = 30)
{
    float R = pow(M/m,1./nbins);
    vector<float> edges;
    edges.reserve(nbins+1);
    for (float edge = m; edge <= M; edge *= R)
        edges.push_back(edge);
    return edges;
}

////////////////////////////////////////////////////////////////////////////////
/// Obtain dimuon spectrum
void getDimuonSpectrum 
       (const vector<fs::path>& inputs, //!< input ROOT files (n-tuples)
        const fs::path& output, //!< output ROOT file (n-tuple)
        const int steering, //!< parameters obtained from explicit options 
        const DT::Slice slice = {1,0} //!< number and index of slice
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    unique_ptr<TChain> tIn = DT::GetChain(inputs);
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = unique_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    auto isMC = metainfo.Get<bool>("flags", "isMC");

    RecEvent * recEvt = nullptr;
    GenEvent * genEvt = nullptr;
    tIn->SetBranchAddress("recEvent", &recEvt);
    if (isMC)
        tIn->SetBranchAddress("genEvent", &genEvt);

    vector<RecMuon> * recMuons = nullptr;
    tIn->SetBranchAddress("recMuons", &recMuons);
    vector<RecPhoton> * recPhotons = nullptr;
    if (branchExists(tIn, "recPhotons"))
        tIn->SetBranchAddress("recPhotons", &recPhotons);
    vector<RecJet> * recJets = nullptr;
    if (branchExists(tIn, "recJets"))
        tIn->SetBranchAddress("recJets", &recJets);

    vector<float> mass_edges = GetMassEdges();
    unique_ptr<TH1F> h_dimuon = make_unique<TH1F>("dimuon", "", mass_edges.size()-1, mass_edges.data());
    unique_ptr<TH1F> h_mmg = make_unique<TH1F>("mumugamma", "", mass_edges.size()-1, mass_edges.data());

    vector<double> pt_edges = {25., 30., 35., 40., 50., 60., 70., 80., 90., 100., 110.,
                                130., 150., 170., 190., 220., 250., 400., 1000.};
    unique_ptr<TH3D> h_Zjet = make_unique<TH3D>("Zjet", "", (int) pt_edges.size()-1, pt_edges.data(),
                                                            (int) y_edges.size()-1, y_edges.data(),
                                                            (int) y_edges.size()-1, y_edges.data());

    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        if (recMuons->size() < 2) continue;

        FourVector m0 = recMuons->at(0).CorrP4(),
                   m1 = recMuons->at(1).CorrP4();
        FourVector diMuon = m0 + m1;
        float w_Z = recEvt->weights.front()
                * recMuons->at(0).weights.front()
                * recMuons->at(1).weights.front();
        if (isMC) w_Z *= genEvt->weights.front();
        h_dimuon->Fill(diMuon.M(), w_Z);

        if (recJets != nullptr && !recJets->empty() && // at least one jet
                abs(diMuon.M()-91.2) < 20 && diMuon.Pt() > 25) {
            const auto& jet = recJets->front().CorrP4();
            if (jet.Pt() > 20 && abs(jet.Rapidity()) < 2.4 && // jet selection
                    min(m0.Pt(), m1.Pt()) > 29 && max(abs(m0.Eta()), abs(m1.Eta())) < 2.4 && // muon selection
                    DeltaR(jet, m0) > 0.4 && DeltaR(jet, m1) > 0.4) { // opening angle
                float w_j = recJets->front().weights.front(),
                      y_b = 0.5*(diMuon.Rapidity() + recJets->front().Rapidity()),
                      y_s = 0.5*(diMuon.Rapidity() - recJets->front().Rapidity());
                h_Zjet->Fill(diMuon.Pt(), y_b, y_s, w_Z * w_j);
            }
        }

        if (recPhotons != nullptr && !recPhotons->empty()) {
            FourVector ph = recPhotons->front().CorrP4();
            FourVector mumugamma = diMuon + ph;
            float w_ph = recPhotons->front().weights.front();
            h_mmg->Fill(mumugamma.M(), w_Z * w_ph);
        }
    }

    metainfo.Set<bool>("git", "complete", true);
    tOut->Write();
    h_dimuon->SetDirectory(fOut.get());
    h_mmg->SetDirectory(fOut.get());
    h_Zjet->SetDirectory(fOut.get());
    h_dimuon->Write();
    h_mmg->Write();
    h_Zjet->Write();

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of namespace DAS::Muon

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Obtain the dimuon mass spectrum.", DT::split);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file");
        options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::Muon::getDimuonSpectrum(inputs, output, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
