#ifndef DAS_UNFOLDING_MJJYMAX
#define DAS_UNFOLDING_MJJYMAX

#include <list>
#include <vector>
#include <optional>
#include <functional>

#include <TUnfoldBinning.h>
#include <TTreeReaderArray.h>
#include <TH1.h>
#include <TH2.h>

#if !defined(__CLING__) || defined(__ROOTCLING__)
#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Di.h"
#include "Core/Unfolding/interface/Observable.h"
#endif

namespace DAS::Unfolding::DijetMass {

static const std::vector<double> Mjj_edges { 200,  249,  306,  372,  449,  539,  641,
                                             756,  887, 1029, 1187, 1361, 1556, 1769,
                                            2008, 2273, 2572, 2915, 3306, 3754, 4244,
                                            4805, 5374, 6094, 6908, 7861, 8929, 10050 },
             ymax_edges{0., 0.5, 1.0, 1.5, 2.0, 2.5, 3.0};

static const size_t nYmaxBins = ymax_edges.size() - 1;

static const auto minMjj = Mjj_edges.front(),
                  maxMjj = Mjj_edges.back(),
                  maxy = ymax_edges.back();

#if !defined(__CLING__) || defined(__ROOTCLING__)
struct MjjYmax final : public Observable {
    int nGenMjjBinsFwd, nGenMjjBinsCnt;

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    MjjYmax ();

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructs a filler for the observable
    std::unique_ptr<Filler> getFiller (TTreeReader& reader) const override;

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Observable::getLmatrix`
    ///
    /// TODO: fix
    void setLmatrix (const std::unique_ptr<TH1>&, std::unique_ptr<TH2>&) override;
};

struct MjjYmaxFiller final : public Filler {
    MjjYmax obs; ///< Backreference to the observable

    std::optional<TTreeReaderArray<GenJet>> genJets;
    TTreeReaderArray<RecJet> recJets;
    std::optional<TTreeReaderValue<GenEvent>> gEv;
    TTreeReaderValue<RecEvent> rEv;

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    MjjYmaxFiller (const MjjYmax &obs, TTreeReader& reader);

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Observable::fillRec`
    std::list<int> fillRec (Variation&) override;

    std::optional<bool> matched;

    ////////////////////////////////////////////////////////////////////////////////
    /// Match the two pairs of leading jets (if any) and set `matched` member.
    void match () override;

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Observable::fillMC`
    void fillMC (Variation&) override;
};
#endif

} // end of DAS::Unfolding::DijetMass namespace
#endif
