#pragma once

#include <list>

#include "Core/Uncertainties/interface/Variation.h"

#include <TString.h>

#include <MetaInfo.h>

class TH1;
class TH2;
class TDirectory;
class TUnfoldBinning;

namespace DAS::Unfolding {

////////////////////////////////////////////////////////////////////////////////
/// Generic structure to hold the histograms and indices corresponding to one
/// variation, for any observable using `TUnfoldBinning`.
///
/// This class is intended to be used in `getUnfHist` only.
struct Variation : public DAS::Uncertainties::Variation {

    static bool isMC; //!< flag from metainfo

    static TUnfoldBinning * genBinning, //!< full binning at particle level
                          * recBinning; //!< full binning at detector level

    TH1 * rec, //!< reconstructed-level distribution
        * tmp, //!< temporary histogram help fill the covariance matrix
        * gen, //!< generated-level distribution
        * missNoMatch, //!< losses (unmatched entries)
        * missOut, //!< losses (migration out of phase space)
        * fakeNoMatch, //!< background (unmatched entries)
        * fakeOut; //!< background (migration out of phase space)
    // TODO: include miss entries in RM
    // TODO: use TUnfoldBinning to decorrelate categories of miss/fake entries
    TH2 * cov, //!< covariance matrix
        * RM; //!< response matrix

    ////////////////////////////////////////////////////////////////////////////////
    /// Destructor
    ~Variation ();

    ////////////////////////////////////////////////////////////////////////////////
    /// Normal constructor
    Variation (const TString& group, const TString& name, size_t index = 0, int bit = 0);

    ////////////////////////////////////////////////////////////////////////////////
    /// Create a subdirectory to the directory given in argument where all
    /// histograms are written.
    void Write (TDirectory * d);
};

////////////////////////////////////////////////////////////////////////////////
/// Fill covariance matrix for a given variation (and its `tmp` histogram).
/// The list allows to directly access the non-trivial elements of the histogram.
void fillCov (const Variation&, const std::list<int>&); // TODO: make this function a static member of Observable

////////////////////////////////////////////////////////////////////////////////
/// Get all variations availables according to metainfo.
std::vector<Variation> GetVariations (Darwin::Tools::MetaInfo&);

} // end of namespace DAS::Unfolding
