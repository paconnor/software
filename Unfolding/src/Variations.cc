#include <cassert>
#include <iostream>
#include <numeric>

#include <TH1.h>
#include <TH2.h>
#include <TDirectory.h>
#include <TUnfoldBinning.h>

#include "Core/Unfolding/interface/Variation.h"

#include <colours.h>

using namespace std;

static const auto feps = numeric_limits<float>::epsilon();

namespace DAS::Unfolding {

Variation::Variation (const TString& group, const TString& name, size_t index, int bit) :
        DAS::Uncertainties::Variation(group, name, index, bit),
        rec(nullptr), tmp(nullptr), gen(nullptr),
        missNoMatch(nullptr), missOut(nullptr), fakeNoMatch(nullptr), fakeOut(nullptr),
        cov(nullptr), RM(nullptr)
{
    cout << "|- Setting up " << __func__ << " `" << name << "`." << endl;

    assert(genBinning != nullptr); // TODO: proper error message
    assert(recBinning != nullptr);

    // TH1 * TUnfoldBinning::CreateHistogram (const char * histogramName,
    //                                        Bool_t       originalAxisBinning = kFALSE, // no physical axis
    //                                        Int_t **     binMap = nullptr, // mapping from global bins to hist bins (not used here)
    //                                        const char * histogramTitle = nullptr,
    //                                        const char * axisSteering = nullptr  // needed for underflow and overflow (not done here)
    //                                       )
    rec = recBinning->CreateHistogram(name + "rec", false, nullptr, "detector level (all)");
    tmp = recBinning->CreateHistogram(name + "tmp", false, nullptr, "temporary object to calculate covariance");

    // TH2D * TUnfoldBinning::CreateErrorMatrixHistogram (const char * histogramName,
    //                                                    Bool_t       originalAxisBinning,
    //                                                    Int_t **     binMap = nullptr,
    //                                                    const char * histogramTitle = nullptr,
    //                                                    const char * axisSteering = nullptr 
    //                                                   )
    cov = recBinning->CreateErrorMatrixHistogram(name + "cov", false, nullptr, "detector level (covariance matrix)");

    if (!isMC) return;
    gen         = genBinning->CreateHistogram(name + "gen"        , false, nullptr, "particle level (all)");
    missNoMatch = genBinning->CreateHistogram(name + "missNoMatch", false, nullptr, "particle level (truly unmatched)");
    missOut     = genBinning->CreateHistogram(name + "missOut"    , false, nullptr, "particle level (matched out of the phase space)");
    fakeNoMatch = recBinning->CreateHistogram(name + "fakeNoMatch", false, nullptr, "detector level (truly unmatched)");
    fakeOut     = recBinning->CreateHistogram(name + "fakeOut"    , false, nullptr, "detector level (matched out of the phase space)");

    // TH2D * TUnfoldBinning::CreateHistogramOfMigrations (TUnfoldBinning const * xAxis,
    //                                                     TUnfoldBinning const * yAxis,
    //                                                     char const *           histogramName,
    //                                                     Bool_t                 originalXAxisBinning = kFALSE,
    //                                                     Bool_t                 originalYAxisBinning = kFALSE,
    //                                                     char const *           histogramTitle = nullptr 
    //                                                    )
    RM  = TUnfoldBinning::CreateHistogramOfMigrations(genBinning, recBinning, name + "RM", false, false, "response matrix");
}

void Variation::Write (TDirectory * d)
{
    cout << "Writing " << name << endl;
    d->cd();
    TDirectory * dd = d->mkdir(name);
    dd->cd();
    for (auto h: {rec,gen,missNoMatch,missOut,fakeNoMatch,fakeOut,
                  dynamic_cast<TH1*>(RM), dynamic_cast<TH1*>(cov)}) {
        if (h == nullptr) continue;
        TString n = h->GetName();
        n.ReplaceAll(name,"");
        h->SetDirectory(dd);
        h->Write(n);
    }
}

bool Variation::isMC = false; // just fort initialisation
TUnfoldBinning * Variation::genBinning = nullptr;
TUnfoldBinning * Variation::recBinning = nullptr;

Variation::~Variation ()
{
    // 1D histograms
    for (auto h: {rec,gen,missNoMatch,missOut,fakeNoMatch,fakeOut}) {
        if (h == nullptr) continue;
        //if (h->GetEntries() == 0) // TODO: fix move constructor (otherwise this is prompted when copying, which is misleading)
        //    cerr << orange << h->GetName() << " is empty" << def << '\n';
        if (h->GetBinContent(0) > 0)
            cerr << orange << h->GetName() << "'s underflow is not empty\n" << def;
        if (h->GetBinContent(h->GetNbinsX()+1) > 0)
            cerr << orange << h->GetName() << "'s overflow is not empty\n" << def;
    }

    // 2D histograms
    for (auto h: {RM, cov}) {
        if (h == nullptr) continue;

        int nBinsX = h->GetNbinsX();
        int nBinsY = h->GetNbinsY();

        bool underflowFilled = false,
             overflowFilled = false;
        for (int i = 1; i <= nBinsX; ++i) {
            underflowFilled |= h->GetBinContent(i,0) > 0;
            overflowFilled |= h->GetBinContent(i,nBinsY+1) > 0;
        }
        if (underflowFilled)
            cerr << orange << h->GetName() << "'s underflow on horizontal axis is not empty\n" << def;
        if (overflowFilled)
            cerr << orange << h->GetName() << "'s overflow on horizontal axis is not empty\n" << def;

        underflowFilled = false;
        overflowFilled = false;
        for (int i = 1; i <= nBinsX; ++i) {
            underflowFilled |= h->GetBinContent(0,i) > 0;
            overflowFilled |= h->GetBinContent(nBinsX+1,0) > 0;
        }
        if (underflowFilled)
            cerr << orange << h->GetName() << "'s underflow on vertical axis is not empty\n" << def;
        if (overflowFilled)
            cerr << orange << h->GetName() << "'s overflow on vertical axis is not empty\n" << def;
    }

    if (isMC) {

        bool genClosure = true, recClosure = true;
        auto checkbin = [](TH1 * h, int i) {
            auto content = h->GetBinContent(i);
            bool ok = std::abs(content - 1) < feps || std::abs(content) < feps;
            if (!ok) cerr << orange << "| " << i << " " << content << "\n" << def;
            return ok;
        };

        auto RMx = RM->ProjectionX();
        RMx->Add(missNoMatch);
        RMx->Add(missOut);
        RMx->Divide(gen);
        for (int i = 1; i <= RMx->GetNbinsX(); ++i)
            genClosure &= checkbin(RMx, i);

        auto RMy = RM->ProjectionY();
        RMy->Add(fakeNoMatch);
        RMy->Add(fakeOut);
        RMy->Divide(rec);
        for (int i = 1; i <= RMy->GetNbinsX(); ++i)
            recClosure &= checkbin(RMy, i);

        if (!recClosure)
            cerr << red << name << " is not closing at rec level" << '\n' << def;
        if (!genClosure)
            cerr << red << name << " is not closing at gen level" << '\n' << def;
    }
}

} // end of DAS::Unfolding namespace

void DAS::Unfolding::fillCov (const DAS::Unfolding::Variation& v, const std::list<int>& binIDs)
{
    for (auto x: binIDs) for (auto y: binIDs) {
        double cCov = v.cov->GetBinContent(x,y),
               cTmp = v.tmp->GetBinContent(x)
                    * v.tmp->GetBinContent(y);
        v.cov->SetBinContent(x,y,cCov+cTmp);
    }
    for (auto x: binIDs) {
        v.tmp->SetBinContent(x, 0);
        v.tmp->SetBinError  (x, 0);
    }
}

vector<DAS::Unfolding::Variation> DAS::Unfolding::GetVariations (Darwin::Tools::MetaInfo& metainfo)
{
    using namespace DAS::Unfolding;

    vector<Variation> variations;
    variations.reserve(100);
    variations.emplace_back(Variation{"nominal", "nominal"});

    const TList * groupList = metainfo.List("variations");
    for (const TObject * group: *groupList) {
        const auto groupContents = dynamic_cast<const TList*>(group);
        if (!groupContents)
            BOOST_THROW_EXCEPTION( invalid_argument(group->GetName()) );
        size_t i = 0;
        for (const TObject * obj: *groupContents) {
            const auto string = dynamic_cast<const TObjString*>(obj);
            if (!string)
                BOOST_THROW_EXCEPTION( invalid_argument(obj->GetName()) );
            cout << group->GetName() << " " << string->GetString() << " " << (i+1) << endl;
            variations.emplace_back(group->GetName(), string->GetString(), ++i);
        }
    }

    return variations;
}
