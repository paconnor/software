#include "Core/Unfolding/interface/BF.h"

#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/variables.h"
#include "Math/VectorUtil.h"
#include <exceptions.h>

using namespace std;
using namespace DAS::Unfolding::ZmmY;
namespace DE = Darwin::Exceptions;

namespace {

inline bool goodMass (float mass) { return 76 <= mass && mass < 106; };

}

BFFiller::BFFiller (const BF& obs, TTreeReader& reader)
    : obs(obs)
    , genMuons(initOptionalBranch<decltype(genMuons)>(reader, "genMuons"))
    , genPhotons(initOptionalBranch<decltype(genPhotons)>(reader, "genPhotons"))
    , recMuons(reader, "recMuons")
    , recPhotons(reader, "recPhotons")
    , gEv(initOptionalBranch<decltype(gEv)>(reader, "genEvent"))
    , rEv(reader, "recEvent")
{
}

template<typename Muon>
bool BFFiller::selection (const TTreeReaderArray<Muon>& muons,
                          const Variation& v)
{
    if (muons.GetSize() < 2) return false;

    const auto& m0 = muons.At(0),
                m1 = muons.At(1);

    return (m0.Q * m1.Q == -1) // opposite charges
        && (v.getCorrP4(m0).Pt() >= 20) // trigger efficiency
        && (v.getCorrP4(m1).Pt() >= 15) // trigger efficiency
        && (abs(m0.p4.Eta()) < 2.4) // muon chamber acceptance
        && (abs(m1.p4.Eta()) < 2.4); // muon chamber acceptance
}

list<int> BFFiller::fillRec (Variation& v)
{
    irecbin.reset();
    recZW.reset();

    if (!selection(recMuons, v)) return {};

    double evW = v.getCorrection(RecEvent::WeightVar, rEv->weights);
    if (obs.isMC) evW *= v.getCorrection(GenEvent::WeightVar, (*gEv)->weights);

    const auto& m0 = recMuons.At(0),
                m1 = recMuons.At(1);

    FourVector Z = v.getCorrP4(m0) + v.getCorrP4(m1);
    float mass = Z.M();

    recZW = v.getWeight(m0) * v.getWeight(m1);

    if (goodMass(mass)) { // fill dimuon mass
        irecbin = obs.recBinning->GetGlobalBinNumber(mass, 1.);
        if (*irecbin == 0) cerr << red << mass << '\n' << def;

        v.tmp->Fill(*irecbin, evW * *recZW);
        v.rec->Fill(*irecbin, evW * *recZW);

        return {*irecbin};
    }

    if (mass >= 40 && recPhotons.GetSize() > 0) { // fill dimuon+gamma mass

        bool found = false;
        for (const auto& p: recPhotons) {

            if (p.p4.Pt() < 20) continue; // TODO: use corrected 4-vec
            if (abs(p.p4.Eta()) > 2.5) continue;

            bool tooClose = false;
            for (const auto& m: recMuons)
                tooClose |= DeltaR(p.p4,m.p4) < 0.1;
            if (tooClose) continue;

            Z = Z + p.CorrP4();
            *recZW *= v.getWeight(p);
            found = true;
        }

        mass = Z.M();
        if (found && goodMass(mass)) {
            irecbin = obs.recBinning->GetGlobalBinNumber(mass, 2.);
            if (*irecbin == 0) cerr << red << mass << '\n' << def;

            v.tmp->Fill(*irecbin, evW * *recZW);
            v.rec->Fill(*irecbin, evW * *recZW);

            return {*irecbin};
        }
    }

    return {};
}

void BFFiller::fillMC (Variation& v)
{
    if (!obs.isMC)
        BOOST_THROW_EXCEPTION( runtime_error(__func__ + " should only be called for MC"s) );

    auto rEvW = v.getCorrection(RecEvent::WeightVar, rEv->weights),
         gEvW = v.getCorrection(GenEvent::WeightVar, (*gEv)->weights);

    optional<int> igenbin;
    optional<double> genZW;

    if (selection(*genMuons, v)) {

        const auto& m0 = genMuons->At(0),
                    m1 = genMuons->At(1);

        FourVector Z = v.getCorrP4(m0) + v.getCorrP4(m1);
        float mass = Z.M();

        genZW = v.getWeight(m0) * v.getWeight(m1);

        if (goodMass(mass)) { // fill dimuon mass
            igenbin = obs.genBinning->GetGlobalBinNumber(*obs.process, 1.);
            if (*igenbin == 0) cerr << red << mass << '\n' << def;
        }
        else if (mass >= 40 && genPhotons->GetSize() > 0) {

            bool found = false;
            for (const auto& p: *genPhotons) {
                if (!p.zAncestor) continue;
                if (p.p4.Pt() < 20) continue;
                if (abs(p.p4.Eta()) >= 2.5)  continue;

                Z = Z + p.p4;
                found = true;
            }
            if (found && goodMass(Z.M())) {
                igenbin = obs.genBinning->GetGlobalBinNumber(*obs.process, 2.);
                if (*igenbin == 0) cerr << red << mass << '\n' << def;
                // no weight for gen photons (TODO?)
            }
        }
    }

    if (igenbin) v.gen->Fill(*igenbin, gEvW * *genZW);

    if (igenbin && irecbin) {
        // Good events
        v.RM     ->Fill(*igenbin, *irecbin, gEvW * *genZW *      rEvW * *recZW);
        v.missOut->Fill(*igenbin,           gEvW * *genZW * (1 - rEvW * *recZW));
    } else if (igenbin && !irecbin)
        // Miss
        v.missOut->Fill(*igenbin,           gEvW * *genZW                      );
    else if (!igenbin && irecbin)
        // Fake
        v.fakeOut->Fill(          *irecbin, gEvW *                rEvW * *recZW);
}

////////////////////////////////////////////////////////////////////////////////

BF::BF () :
    Observable(__FUNCTION__, "Branching fraction of Z decaying to two muons and a photon")
{
    int nCh = channels.size(),
        nPr = processes.size();
    recBinning->AddAxis("invMass"   ,  30,  76,     106,false,false);
    recBinning->AddAxis("finalState", nCh, 0.5, 0.5+nCh,false,false);
    genBinning->AddAxis("processes" , nPr, 0.5, 0.5+nPr,false,false);
    genBinning->AddAxis("finalState", nCh, 0.5, 0.5+nCh,false,false);
}

unique_ptr<DAS::Unfolding::Filler> BF::getFiller (TTreeReader& reader) const
{
    return make_unique<BFFiller>(*this, reader);
}
