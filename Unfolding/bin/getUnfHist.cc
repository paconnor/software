#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"

#include "Core/Unfolding/interface/PtY.h"
#include "Core/Unfolding/interface/MjjYmax.h"
#include "Core/Unfolding/interface/HTn.h"
#include "Core/Unfolding/interface/Variation.h"

#include "Math/VectorUtil.h"
#include "Math/GenVector/GenVector_exception.h"

#include <darwin.h>

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Unfolding {

////////////////////////////////////////////////////////////////////////////////
/// Get histograms for unfolding only for pt (in d=1), 
/// without using `TUnfoldBinning`.
///
/// Then the output can be unfolded using `unfold`.
void getUnfHist
        (const vector<fs::path>& inputs, //!< input ROOT files (n-tuple)
         const fs::path& output, //!< output ROOT file
         const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
         const int steering, //!< parameters obtained from explicit options
         const DT::Slice slice = {1,0} //!< number and index of slice
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    unique_ptr<TChain> tIn = DT::GetChain(inputs);
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = unique_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    bool isMC = metainfo.Get<bool>("flags", "isMC");
    int R = metainfo.Get<int>("flags", "R");

    TTreeReader reader(tIn.get());

    cout << "Setting observables" << endl;
    Observable::isMC = isMC;
    Observable::maxDR = R / 20.;
    auto pt_obs = config.get_child("unfolding.observables");
    vector<Observable *> observables = GetObservables(pt_obs);
    vector<unique_ptr<Filler>> fillers;
    for (const auto obs: observables)
        fillers.push_back(obs->getFiller(reader));

    cout << "Setting variations" << endl;
    Variation::isMC = isMC;
    Variation::genBinning = new TUnfoldBinning("gen");
    Variation::recBinning = new TUnfoldBinning("rec");
    for (Observable * obs: observables) {
        Variation::genBinning->AddBinning(obs->genBinning);
        Variation::recBinning->AddBinning(obs->recBinning);
    }
    vector<Variation> variations = (steering & DT::syst) == DT::syst
                    ? GetVariations(metainfo)
                    : vector<Variation>{ Variation("nominal", "nominal") };

    // See Darwin::Tools::Looper constructor
    // TODO First-class support in Darwin
    const auto entries = reader.GetEntries();
    const long long start = entries * ((slice.second + 0.) / slice.first);
    const long long stop  = entries * ((slice.second + 1.) / slice.first);
    reader.SetEntriesRange(start, stop);
    while (reader.Next()) {
        if (auto status = reader.GetEntryStatus(); status != TTreeReader::kEntryValid)
            BOOST_THROW_EXCEPTION(
                DE::BadInput(Form("Loading entry %lld failed: error %d",
                                  reader.GetCurrentEntry(), status),
                             tIn) );

        [[ maybe_unused ]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        for (Variation& v: variations) {

            list<int> binIDs; // collect filled bins to calculate covariance
            for (auto& filler: fillers)
                binIDs.merge(filler->fillRec(v));
            fillCov(v, binIDs);

            if (!isMC) continue;

            // run matching (possibly different for each observable)
            for (auto& filler: fillers)
                filler->match();

            // fill gen, RM, fake, miss
            for (auto& filler: fillers)
                filler->fillMC(v);
        }
    }

    fOut->cd();
    for (Variation& v: variations)
        v.Write(fOut.get());

    fOut->cd();
    tOut->Write();

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of namespace DAS::Unfolding

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");
        DT::MetaInfo::versions["TUnfold"] = TUnfold::GetTUnfoldVersion();

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Fill histograms to run global unfolding of "
                            "multiple observables.", DT::syst | DT::split | DT::config);

        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file"               )
               .args("observables", "unfolding.observables", "list of observables (use DAS::Unfolding subnamespaces)");

        const auto& config = options(argc,argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::Unfolding::getUnfHist(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
