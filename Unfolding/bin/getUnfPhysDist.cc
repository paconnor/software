#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <array>
#include <filesystem>
#include <functional>
#include <algorithm>
#include <type_traits>

#include <TString.h>
#include <TFile.h>
#include <TH1.h>
#include <TKey.h>
#include <TVectorT.h>

#include "Math/VectorUtil.h"

#include "Core/Unfolding/interface/Observable.h"

#include <darwin.h>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Unfolding {

////////////////////////////////////////////////////////////////////////////////
/// Two functors and two functions to extract the distributions and matrices
/// with physical axes from bin IDs.
///
/// The two functions allow to loop over
/// - the nodes of a global binning
/// - and the axes, regardless of the dimensionality.
///
/// The two functors are similar:
/// - the first one just handles 1D distribution, and is a bit trivial, but 
///   necessary to allow the second one;
/// - the second one can keep in memory the result of the loops on the first 
///   axis and triggers a secound round of loops for the second axis.
namespace ExtractHistogram {

////////////////////////////////////////////////////////////////////////////////
/// Functor to extract a 1D distribution with a physical axis from a ND 
/// distribution with bin IDs.
///
/// Intended to be used together with `axisLoop` and `nodeLoop`.
struct Dist {
    TH1 * const hIn; //!< input histogram with bin IDs

    void operator() (TDirectory * dOut, //!< output directory   
                     TString name, //!< hist name in output directory
                     TUnfoldBinning * node, //!< local node
                     vector<double> x, //!< physical values
                     TString title) //!< simplistic title, based on bin edges
    {
        const TVectorD * edges = node->GetDistributionBinning(0);
        int nbins = edges->GetNrows()-1;
        TH1F hOut(name, title, nbins, edges->GetMatrixArray());
        for (int ibin = 1; ibin <= nbins; ++ibin) {
            x.front() = hOut.GetBinLowEdge(ibin);
            int Ibin = node->GetGlobalBinNumber(x.data());
            auto content = hIn->GetBinContent(Ibin);
            hOut.SetBinContent(ibin, content);
        }
        hOut.SetDirectory(dOut);
        hOut.SetMinimum(hIn->GetMinimum());
        hOut.SetMaximum(hIn->GetMaximum());
        hOut.Write();
    }
};

////////////////////////////////////////////////////////////////////////////////
/// Functor to extract a 2D distribution with physical axes from a 2ND 
/// distribution with bin IDs.
///
/// Intended to be used together with `axisLoop` and `nodeLoop`.
struct Matrix {
    TH2 * const hIn; //!< input histogram with bin IDs
    TUnfoldBinning * binning2; //!< global binning (2nd axis)

    TString name1; //!< partial name (w.r.t. 1st axis)
    TUnfoldBinning * node1; //!< local node (1st axis)
    vector<int> ibins1; //!<  global bin IDs of present iteration
    vector<double> x1; //!< physical values (1st axis)
    TString title1; //!< simplistic title (1st axis)

    Matrix (TH2 * h, TUnfoldBinning * bng2) :
        hIn(h), binning2(bng2), node1(nullptr)
    { }

    void operator() (TDirectory * dOut, //!< output directory
                     TString name2, //!< partial name (w.r.t. 2nd axis)
                     TUnfoldBinning * node2, //!< local node (2nd axis)
                     vector<double> x2, //!< physical values (2nd axis)
                     TString title2) //!< simplistic title, based on bin edges
    {
        const TVectorD * edges1 = node1->GetDistributionBinning(0),
                       * edges2 = node2->GetDistributionBinning(0);
        int nbins1 = edges1->GetNrows()-1,
            nbins2 = edges2->GetNrows()-1;
        TString name = name1 + "__" + name2,
                title = title1 + "-- " + title2;

//    const char * name = Form("%s_%s"           , X.h->GetName()             , Y.h->GetName()             ),
//               * title = Form("%s, %s;%s;%s;%s", X.h->GetTitle()            , Y.h->GetTitle()            ,
//                                                 X.h->GetXaxis()->GetTitle(), Y.h->GetXaxis()->GetTitle(),
//                                                 h->GetYaxis()->GetTitle()                               );

        TH2F hOut(name, title, nbins1, edges1->GetMatrixArray(),
                               nbins2, edges2->GetMatrixArray());
        for (int ibin1 = 1; ibin1 <= nbins1; ++ibin1)
        for (int ibin2 = 1; ibin2 <= nbins2; ++ibin2) {
            x1.front() = hOut.GetXaxis()->GetBinLowEdge(ibin1);
            x2.front() = hOut.GetYaxis()->GetBinLowEdge(ibin2);
            int Ibin1 = node1->GetGlobalBinNumber(x1.data()),
                Ibin2 = node2->GetGlobalBinNumber(x2.data());
            auto content = hIn->GetBinContent(Ibin1, Ibin2);
            hOut.SetBinContent(ibin1, ibin2, content);
        }
        hOut.SetDirectory(dOut);
        hOut.SetMinimum(hIn->GetMinimum());
        hOut.SetMaximum(hIn->GetMaximum());
        hOut.Write();
    }
};

template<typename T> void nodeLoop (TDirectory *, TUnfoldBinning *, T, TString = "", int = 1);

////////////////////////////////////////////////////////////////////////////////
/// Loop on the axes of the node of a given binning.
template<typename T> //!< either `Dist` or `Matrix`
void axisLoop (TDirectory * dOut, //!< output directory
               TUnfoldBinning * node, //!< child node
               T t, //!< expecting either a `Dist` or a `Matrix` object
               vector<int> ibins, //!< global bin IDs of present iteration
               int shift, //!< shift to continue the bin index despite the change of node/binning
               TString name = "",
               TString title = "")
{
    int iaxis = count_if(next(begin(ibins)), end(ibins),
                         [](int ibin) { return ibin == 0; });
    int& ibin = ibins[iaxis];
    assert(ibin == 0);
    if (iaxis > 0) {
        TString axisname = node->GetDistributionAxisLabel(iaxis);
        cout << "\t\taxis `" << axisname << "`" << endl;
        name += "_" + axisname + "bin";
        const TVectorD * edges = node->GetDistributionBinning(iaxis);
        for (ibin = 1; ibin < edges->GetNrows(); ++ibin) {
            TString nameExt = name + (ibin + shift),
                    titleExt = title + Form("[%f, %f] ", (*edges)[ibin-1], (*edges)[ibin]);
            axisLoop(dOut, node, t, ibins, shift, nameExt, titleExt);
        }
    }
    else {
        cout << "\t\t\tpreparing `" << name << "`" << endl;
        size_t naxes = ibins.size();
        vector<double> x(naxes);
        for (size_t jaxis = 1; jaxis < naxes; ++jaxis) {
            const TVectorD * edges = node->GetDistributionBinning(jaxis);
            int ibin = ibins[jaxis];
            assert(ibin > 0);
            x[jaxis] = (*edges)[ibin-1]; // vector component (hist bin) index starts from 0 (1)
        }

        if constexpr (is_same_v<T,Dist>)
            t(dOut, name, node, x, title);
        else if constexpr (is_same_v<T,Matrix>) {
            if (t.node1 == nullptr) {
                t.name1 = name;
                t.node1 = node;
                t.ibins1 = ibins;
                t.x1 = x;
                t.title1 = title;
                nodeLoop<T>(dOut, t.binning2, t);
            }
            else
                t(dOut, name, node, x, title);
        }
        else {
            cerr << red << "Unexpected type. Skipping.\n" << def;
            return;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
/// Loop on the nodes of a given binning.
template<typename T> //!< either `Dist` or `Matrix`
void nodeLoop (TDirectory * dOut, //!< output directory
               TUnfoldBinning * binning, //!< global binning or parent node
               T t, //!< expecting either a `Dist` or a `Matrix` object
               TString name, //!< name of output directory
               int depth) //!< trick to know the number of recursive calls
{
    int shift = 0;
    for (auto node = binning->GetChildNode(); node != nullptr;
              node = node->GetNextNode()) {
        TString dirname = name + node->GetName();
        if (node->GetDistributionNumberOfBins() == 0) {
            nodeLoop(dOut, node, t, dirname, depth+1);
            continue;
        }
        cout << "\tnode `" << dirname << "`" << endl;
        auto ddOut = dOut->mkdir(dirname, node->GetTitle(), true /* = `-p` */);
        ddOut->cd();

        int dim = node->GetDistributionDimension();
        TString axisname = node->GetDistributionAxisLabel(0);
        axisLoop<T>(ddOut, node, t, vector<int>(dim,0), shift, axisname);
        if (depth > 1) { // NOTE: this logic is tuned for dijet mass, with different resolution in central and forward regions
            const TVectorD * edges = node->GetDistributionBinning(1 /* TODO: d > 2?*/);
            shift += edges->GetNrows()-1;
        }
    } // end of loop on nodes
}

} // end of `ExtractHistogram` namespace

////////////////////////////////////////////////////////////////////////////////
/// Extract distributions with physical binning from output of the various
/// `getUnf*` commands.
void getUnfPhysDist
           (const fs::path& input, //!< input ROOT file (histograms)
            const fs::path& output, //!< output ROOT file (histograms)
            const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
            const int steering //!< parameters obtained from explicit options 
            )
{
    cout << __func__ << " start" << endl;

    cout << "Setting observables" << endl;
    auto pt_obs = config.get_child("unfolding.observables");
    vector<Observable *> observables = GetObservables(pt_obs);

    cout << "Setting up axes" << endl;
    auto genBinning = new TUnfoldBinning("gen"),
         recBinning = new TUnfoldBinning("rec");
    for (Observable * obs: observables) {
        genBinning->AddBinning(obs->genBinning);
        recBinning->AddBinning(obs->recBinning);
    }

    unique_ptr<TFile> fIn(TFile::Open(input.c_str(), "READ")),
                      fOut(DT_GetOutput(output));

    bool applySyst = (steering & DT::syst) != DT::syst;

    cout << "Looping over input `TDirectory`s (which in principle correspond to variations)" << endl;
    for (const auto&& obj: *(fIn->GetListOfKeys())) {
        cout << obj->GetName() << endl;
        auto key = dynamic_cast<TKey*>(obj);
        const TString classname = key->ReadObj()->ClassName();
        if (!classname.Contains("TDirectory")) continue;
        auto dIn = dynamic_cast<TDirectory*>(key->ReadObj());
        TString dirname = dIn->GetName();
        if (!applySyst && dirname != "nominal") continue;
        auto dOut = fOut->mkdir(dIn->GetName());
        dIn->cd();
        dOut->SetTitle(dIn->GetTitle());

        map<TString, TUnfoldBinning *> hists {
            {"rec"        , recBinning},
            {"recMC"      , recBinning},
            {"recData"    , recBinning},
            {"backfold"   , recBinning},
            {"genMC"      , genBinning},
            {"unfold"     , genBinning},
            {"fakeNoMatch", recBinning},
            {"fakeOut"    , recBinning},
            {"missNoMatch", genBinning},
            {"missOut"    , genBinning},
            {"purity"     , recBinning},
            {"stability"  , genBinning}
        };
        cout << "Looping over input `TH1`s" << endl;
        for (auto hist: hists) {
            TString name = hist.first;
            TUnfoldBinning * binning = hist.second;

            auto h = dIn->Get<TH1>(name);
            if (h == nullptr) {
                cerr << orange << "Could not find `" << name << "`. Skipping.\n" << def;
                continue;
            }

            auto ddOut = dOut->mkdir(name, h->GetTitle());
            ExtractHistogram::Dist dist { h };
            ExtractHistogram::nodeLoop(ddOut, binning, dist);
        }

        map<TString, pair<TUnfoldBinning *, TUnfoldBinning *>> hist2Ds {
            {"cov"         , {recBinning, recBinning}},
            {"corr"        , {recBinning, recBinning}},
            {"corrInData"  , {recBinning, recBinning}},
            {"covInData"   , {recBinning, recBinning}},
            {"corrOutTotal", {genBinning, genBinning}},
            {"covOutTotal" , {genBinning, genBinning}},
            {"RM"          , {genBinning, recBinning}},
            {"PM"          , {genBinning, recBinning}}
        };
        cout << "Looping over input `TH2`s" << endl;
        for (auto hist: hist2Ds) {
            TString name = hist.first;
            TUnfoldBinning * binning1 = hist.second.first,
                           * binning2 = hist.second.second;

            auto h = dIn->Get<TH2>(name); // TODO: smart pointer
            if (h == nullptr) {
                cerr << orange << "Could not find `" << name << "`. Skipping.\n" << def;
                continue;
            }

            auto ddOut = dOut->mkdir(name, dIn->GetTitle());
            ExtractHistogram::Matrix mat(h, binning2);
            ExtractHistogram::nodeLoop(ddOut, binning1, mat);
        }
    }

    cout << __func__ << " stop" << endl;
}

} // end of DAS::Unfolding namespace
#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");
        DT::MetaInfo::versions["TUnfold"] = TUnfold::GetTUnfoldVersion();

        fs::path input, output;

        DT::Options options("Extract distributions with physical binning. The structure "
                            "of the input file is preserved, with one directory per "
                            "systematic variation and further subdirectories. For TH1s: "
                            "`variation/distribution/observable/histograms`. For TH2s:" 
                            "`variation/distribution/observable/observable/histograms`.",
                            DT::config | DT::syst);
        options.input ("input" , &input , "input ROOT file")
               .output("output", &output, "output ROOT file")
               .args("observables", "unfolding.observables", "list of observables " // TODO: automate this by looking up the metainfo
                                                             "(consistent with input of `getUnfHist`)");
        const auto& config = options(argc, argv);
        const int steering = options.steering();

        DAS::Unfolding::getUnfPhysDist(input, output, config, steering);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
