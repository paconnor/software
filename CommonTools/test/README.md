# Tests in CMSSW

After compilation, executables are in `$CMSSW_BASE/test/$SCRAM_ARCH`.
You may run them using `scram b runtests` *after* the compilation, or execute them by yourself.
Note that exit errors are not caught.

C++ tests should be implemented like in the `bin` directory.
Scripts may be tested with a [standard hack](https://github.com/cms-sw/cmssw/tree/master/FWCore/Utilities/test).

Documentation:
 - [Developer's guide](https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideDevelopersGuide)
