#ifndef DOXYGEN_SHOULD_SKIP_THIS
#include "Core/CommonTools/interface/GenericSFApplier.h"

#define BOOST_TEST_MODULE testGenericSFApplier
#include <boost/test/included/unit_test.hpp>

#include <TFile.h>
#include <TH1D.h>

#include <filesystem>
#include <utility>
#include <stdexcept>
#include <vector>

using namespace DAS;
using namespace std;
namespace fs = std::filesystem;
namespace utf = boost::unit_test;

#define FILE_NAME       "scaleFactors.root"
#define HIST_PREFIX     "H"
#define BIN_WISE_NAME   "stat"
#define GLOBAL_NAME     "syst"

const double BIN1_SF = 2, BIN2_SF = 3;
const double BIN1_BW = 0.2, BIN2_BW = 0.3;
const double BIN1_GB = 0.5, BIN2_GB = 0.6;

/**
 * \brief Fixture that creates a temporary ROOT file with "scale factor"
 *        histograms inside.
 */
struct HistogramFileFixture
{
    string folder;
    string path;

    HistogramFileFixture ()
    {
        // Unfortunately TFile cannot use file handles directly, so we need to
        // create one on disk
        folder = fs::temp_directory_path() / "test_XXXXXX";
        if (mkdtemp(folder.data()) != folder.data()) {
            throw runtime_error("could not create temp folder");
        }
        path = folder + "/" + FILE_NAME;

        auto file = make_unique<TFile>(path.c_str(), "RECREATE");

        auto h = make_unique<TH1D>(HIST_PREFIX, "", 2, 1, 3);
        h->SetBinContent(1, BIN1_SF);
        h->SetBinContent(2, BIN2_SF);
        h->Write();

        h->SetName(HIST_PREFIX BIN_WISE_NAME);
        h->SetBinContent(1, BIN1_BW);
        h->SetBinContent(2, BIN2_BW);
        h->Write();

        h->SetName(HIST_PREFIX GLOBAL_NAME);
        h->SetBinContent(1, BIN1_GB);
        h->SetBinContent(2, BIN2_GB);
        h->Write();

        file->Write();
    }

    ~HistogramFileFixture ()
    {
        // Delete the temp folder and the file
        fs::remove_all(folder);
    }
};

/**
 * Simple weighted object to use in tests
 */
struct SimpleObject
{
    int bin;
    vector<Weight> weights;
};

/**
 * Simple applier class. Selects objects with bin > 1. Has two uncertainties.
 */
struct SimpleObjectApplier : GenericSFApplier<SimpleObject>
{
    SimpleObjectApplier (const fs::path& filePath, bool correction, bool uncertainties)
        : GenericSFApplier(filePath, correction, uncertainties)
    {
        loadNominal(HIST_PREFIX);
        loadBinWiseUnc(BIN_WISE_NAME, HIST_PREFIX BIN_WISE_NAME);
        loadGlobalUnc(GLOBAL_NAME, HIST_PREFIX GLOBAL_NAME);
    }

    bool passes (const SimpleObject& obj) const override
    { return obj.bin > 1; }

    int binIndex (const SimpleObject& obj,
                  const unique_ptr<TH1>& hist) const override
    { return hist->FindBin(obj.bin); }
};

/**
 * Applier class with context. Use unique_ptr to check that we don't
 * inadvertently copy (since unique_ptr cannot be copied).
 */
struct ContextApplier : GenericSFApplier<SimpleObject, unique_ptr<int>>
{
    /// The test passes this pointer and we check that we receive it correctly.
    unique_ptr<int> expectedContext = make_unique<int>(42);

    ContextApplier (const fs::path& filePath, bool correction, bool uncertainties)
        : GenericSFApplier(filePath, correction, uncertainties)
    {
        loadNominal(HIST_PREFIX);
    }

    bool passes (const SimpleObject&, const unique_ptr<int>& ctx) const override
    {
        BOOST_TEST( ctx.get() == expectedContext.get() );
        return true;
    }

    int binIndex (const SimpleObject& obj,
                  const unique_ptr<int>& ctx,
                  const unique_ptr<TH1>& hist) const override
    {
        BOOST_TEST( ctx.get() == expectedContext.get() );
        return hist->FindBin(obj.bin);
    }
};

/**
 * Object that needs a weightsRef() specialization.
 */
struct SpecialObject
{
    int canary; // Used in testing
    vector<Weight> w; // Weights with an unconventional name
};

/**
 * Specialization of weightsRef() for the special object.
 */
template<>
vector<Weight>& DAS::weightsRef(SpecialObject& obj)
{
    obj.canary = 42; // notify that we were called
    return obj.w;
}

/**
 * Applier class for the SpecialObject.
 */
struct SpecialObjectApplier : GenericSFApplier<SpecialObject>
{
    SpecialObjectApplier () : GenericSFApplier("", false, false)
    {}

    bool passes (const SpecialObject&) const override
    { return true; }

    int binIndex (const SpecialObject&, const unique_ptr<TH1>&) const override
    { return 1; }
};

BOOST_AUTO_TEST_SUITE( GenericSFApplier, * utf::tolerance(0.00001) )

    BOOST_AUTO_TEST_CASE( SimpleObjectNoSF )
    {
        // This applier must not even try to open the file, so passing a dummy
        // file name must be fine
        const SimpleObjectApplier applier("/nowhere", false, false);

        // Must only return systematics that are actually applied
        BOOST_TEST( applier.weightNames().empty() );

        // Selected object
        SimpleObject obj = {2, {}};
        applier(obj);
        BOOST_TEST( obj.weights.size() == 1 );
        BOOST_TEST( obj.weights.at(0).v == 1 );
        BOOST_TEST( obj.weights.at(0).i == 0 );

        // Selected object with existing weights
        obj = {2, {Weight{1, 0}, Weight{2, 1}}};
        applier(obj);
        BOOST_TEST( obj.weights.size() == 2 );
        BOOST_TEST( obj.weights.at(0).v == 1 );
        BOOST_TEST( obj.weights.at(0).i == 0 );
        BOOST_TEST( obj.weights.at(1).v == 2 );
        BOOST_TEST( obj.weights.at(1).i == 1 );

        // Object not selected
        obj = {1, {}}; 
        applier(obj);
        BOOST_TEST( obj.weights.size() == 1 );
        BOOST_TEST( obj.weights.at(0).v == 0 );
        BOOST_TEST( obj.weights.at(0).i == 0 );

        // Object not selected with existing weights
        obj = {1, {Weight{1, 0}, Weight{2, 1}, Weight{3, -1}}};
        applier(obj);
        BOOST_TEST( obj.weights.size() == 1 );
        BOOST_TEST( obj.weights.at(0).v == 0 );
        BOOST_TEST( obj.weights.at(0).i == 0 );
    }

    BOOST_AUTO_TEST_CASE( Vector )
    {
        // This applier must not even try to open the file, so passing a dummy
        // file name must be fine
        const SimpleObjectApplier applier("/nowhere", false, false);

        // First object not selected, second selected
        vector<SimpleObject> objs = {{1, {}}, {2, {}}};
        applier(objs);
        BOOST_TEST( objs.size() == 2 );
        BOOST_TEST( objs.at(0).weights.size() == 1 );
        BOOST_TEST( objs.at(0).weights.at(0).v == 0 );
        BOOST_TEST( objs.at(0).weights.at(0).i == 0 );
        BOOST_TEST( objs.at(1).weights.size() == 1 );
        BOOST_TEST( objs.at(1).weights.at(0).v == 1 );
        BOOST_TEST( objs.at(1).weights.at(0).i == 0 );
    }

    BOOST_AUTO_TEST_CASE( SimpleObjectApplySF )
    {
        // Check error condition
        BOOST_CHECK_THROW( SimpleObjectApplier("/nowhere", true, false),
                           fs::filesystem_error );

        HistogramFileFixture f;
        // This applier must apply the SF without doing systematics
        const SimpleObjectApplier applier(f.path, true, false);

        // Must only return systematics that are actually applied
        BOOST_TEST( applier.weightNames().empty() );

        // Selected object
        SimpleObject obj = {2, {}};
        applier(obj);
        BOOST_TEST( obj.weights.size() == 1 );
        BOOST_TEST( obj.weights.at(0).v == BIN2_SF );
        BOOST_TEST( obj.weights.at(0).i == 0 );

        // Selected object with existing weights
        obj = {2, {Weight{1, 0}, Weight{2, 1}}};
        applier(obj);
        BOOST_TEST( obj.weights.size() == 2 );
        BOOST_TEST( obj.weights.at(0).v == 1 * BIN2_SF );
        BOOST_TEST( obj.weights.at(0).i == 0 );
        BOOST_TEST( obj.weights.at(1).v == 2 * BIN2_SF );
        BOOST_TEST( obj.weights.at(1).i == 1 );

        // Selected object outside histogram bounds
        obj = {10, {}};
        applier(obj);
        BOOST_TEST( obj.weights.size() == 1 );
        BOOST_TEST( obj.weights.at(0).v == 0 );
        BOOST_TEST( obj.weights.at(0).i == 0 );

        // Selected object outside histogram bounds, existing weights
        obj = {10, {Weight{1, 0}, Weight{2, 1}}};
        applier(obj);
        BOOST_TEST( obj.weights.size() == 2 );
        BOOST_TEST( obj.weights.at(0).v == 0 );
        BOOST_TEST( obj.weights.at(0).i == 0 );
        BOOST_TEST( obj.weights.at(1).v == 0 );
        BOOST_TEST( obj.weights.at(1).i == 1 );

        // Object not selected
        obj = {1, {}};
        applier(obj);
        BOOST_TEST( obj.weights.size() == 1 );
        BOOST_TEST( obj.weights.at(0).v == 0 );
        BOOST_TEST( obj.weights.at(0).i == 0 );
    }

    BOOST_AUTO_TEST_CASE( SimpleObjectApplySFAndUnc )
    {
        // Check error condition
        BOOST_CHECK_THROW( SimpleObjectApplier("/nowhere", true, true),
                           fs::filesystem_error );

        HistogramFileFixture f;
        // This applier must apply the SF and do systematics
        const SimpleObjectApplier applier(f.path, true, true);

        // Must only return systematics that are actually applied
        const vector<string> expected_names{
            BIN_WISE_NAME + SysUp, BIN_WISE_NAME + SysDown,
            GLOBAL_NAME + SysUp, GLOBAL_NAME + SysDown};
        BOOST_TEST( applier.weightNames() == expected_names );

        // Selected object
        SimpleObject obj = {2, {}};
        applier(obj);
        BOOST_TEST( obj.weights.size() == 5 );
        BOOST_TEST( obj.weights.at(0).v == BIN2_SF );
        BOOST_TEST( obj.weights.at(0).i == 0 );
        BOOST_TEST( obj.weights.at(1).v == BIN2_SF + BIN2_BW );
        BOOST_TEST( obj.weights.at(1).i == 2 );
        BOOST_TEST( obj.weights.at(2).v == BIN2_SF - BIN2_BW );
        BOOST_TEST( obj.weights.at(2).i == 2 );
        BOOST_TEST( obj.weights.at(3).v == BIN2_SF + BIN2_GB );
        BOOST_TEST( obj.weights.at(3).i == 0 );
        BOOST_TEST( obj.weights.at(4).v == BIN2_SF - BIN2_GB );
        BOOST_TEST( obj.weights.at(4).i == 0 );

        // Selected object with existing weights
        // Check that existing weights are modified as expected
        obj = {2, {Weight{1, 0}, Weight{2, 1}}};
        applier(obj);
        BOOST_TEST( obj.weights.size() == 6 );
        BOOST_TEST( obj.weights.at(0).v == 1 * BIN2_SF );
        BOOST_TEST( obj.weights.at(0).i == 0 );
        BOOST_TEST( obj.weights.at(1).v == 2 * BIN2_SF );
        BOOST_TEST( obj.weights.at(1).i == 1 );

        // Object not selected
        // Check that systematics are not recorded
        obj = {1, {}};
        applier(obj);
        BOOST_TEST( obj.weights.size() == 1 );
        BOOST_TEST( obj.weights.at(0).v == 0 );
        BOOST_TEST( obj.weights.at(0).i == 0 );
    }

    BOOST_AUTO_TEST_CASE( Context )
    {
        // This applier must not even try to open the file, so passing a dummy
        // file name must be fine
        const ContextApplier applier("/nowhere", false, false);

        SimpleObject obj = {2, {}};
        applier(obj, applier.expectedContext);
    }

    BOOST_AUTO_TEST_CASE( SpecialObjectCanary )
    {
        // This applier must not even try to open the file, so passing a dummy
        // file name must be fine
        const SpecialObjectApplier applier;

        SpecialObject obj = {0, {}};
        applier(obj);
        // This checks that the appropriate weightsRef() was called
        BOOST_TEST( obj.canary == 42 );
    }

BOOST_AUTO_TEST_SUITE_END()

#endif // DOXYGEN_SHOULD_SKIP_THIS
