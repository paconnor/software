#ifndef DAS_TOOLBOX
#define DAS_TOOLBOX

#include <vector>
#include <iostream>
#include <cassert>

#include <TList.h>
#include <TString.h>
#include <TCanvas.h>
#include <TChain.h>
#include <TParameter.h>

#include "Math/PtEtaPhiM4D.h"
#include "Math/Point3D.h"
#include "Math/VectorUtil.h"

#include "Core/Objects/interface/Jet.h"

namespace DAS {

inline bool pt_sort (const RecJet& j1, const RecJet& j2)
{
    return j1.CorrPt() > j2.CorrPt();
}

////////////////////////////////////////////////////////////////////////////////
/// Check if branch exists
///
/// Loop over branches of `tree` and check if `brName` exists
template<typename TTreePtr> bool branchExists (const TTreePtr& tree, TString brName)
{
    auto brList = tree->GetListOfBranches();
    bool brFound = false;
    for (auto it = brList->begin(); it != brList->end(); ++it) {
        TString name = (*it)->GetName();
        if (name == brName) brFound = true;
    }
    return brFound;
}

} // end of DAS::namespace

#endif
