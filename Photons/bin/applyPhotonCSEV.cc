#include <cstdlib>
#include <iostream>
#include <vector>
#include <filesystem>

#include "Core/Objects/interface/Photon.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/GenericSFApplier.h"
#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"

#include <TChain.h>
#include <TFile.h>

#include <darwin.h>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Photon {

/**
 * \brief Applies the Conversion Safe Electron Veto and corrections
 *
 * Modifies the weights of the reconstructed photons to apply the efficiency
 * correction. Scale factors are taken from a ROOT file in the format provided
 * by Egamma for UL scale factors.
 */
class CSEVApplier : public GenericSFApplier<DAS::RecPhoton> {
public:
    CSEVApplier (const fs::path& filePath, const string& ID,
                 bool correction, bool uncertainties)
        : GenericSFApplier(filePath, correction, uncertainties)
    {
        /*
         * Meta info: Loose | Medium | Tight | MVA80 | MVA90
         * Egamma conveners believe that the 'MVA' working point in the files
         * corresponds to MVA90. Reference:
         *   https://mattermost.web.cern.ch/cmseg/pl/yewftned9b8bxg4m3y7gdck4ze
         */
        string IDName = ID + "ID";
        if (ID == "MVA80")
            BOOST_THROW_EXCEPTION(std::invalid_argument(
                "There are no CSEV scale factors for the MVA80 ID."));
        else if (ID == "MVA90")
            IDName = "MVAID";

        /*
         * root [1] .ls
         * TFile**         /nfs/dust/cms/user/mourelou/darwin/Darwin/tables/Egamma/Photon_CSEV_UL18.root
         *  TFile*         /nfs/dust/cms/user/mourelou/darwin/Darwin/tables/Egamma/Photon_CSEV_UL18.root
         *   KEY: TDirectoryFile   LooseID;1       LooseID
         *   KEY: TDirectoryFile   MediumID;1      MediumID
         *   KEY: TDirectoryFile   TightID;1       TightID
         *   KEY: TDirectoryFile   MVAID;1 MVAID
         * root [2] LooseID->ls()
         * TDirectoryFile*         LooseID LooseID
         *  KEY: TH1F      SF_CSEV_LooseID;1
         *  KEY: TH1F      Staunc_CSEV_LooseID;1
         *  KEY: TH1F      PUunc_CSEV_LooseID;1
         *  KEY: TH1F      Modelunc_CSEV_LooseID;1
         */
        loadNominal(IDName + "/SF_CSEV_" + IDName);
        loadBinWiseUnc("CSEV_stat", IDName + "/Staunc_CSEV_" + IDName);
        loadGlobalUnc("CSEV_PU",    IDName + "/PUunc_CSEV_" + IDName);
        loadGlobalUnc("CSEV_model", IDName + "/Modelunc_CSEV_" + IDName);

        cout << "Initialisation of " << __func__ << " done" << endl;
    }

protected:
    bool passes (const RecPhoton& photon) const override
    {
        const auto CSEV = RecPhoton::ConversionSafeElectronVeto;
        return (CSEV & photon.selectors) == CSEV;
    }

    int binIndex (const RecPhoton& photon,
                  const std::unique_ptr<TH1>& hist) const override
    {
        /*
         * The axis is categorical:
         *   0 = EB inclusive
         *   1 = EB high R9
         *   2 = EB low R9
         *   3 = EE inclusive
         *   4 = EE high R9
         *   5 = EE low R9
         * The threshold for high R9 is 0.96 (using the "shifted" value).
         * Since we don't have R9 in the ntuples we use the inclusive.
         */
        return hist->FindBin(abs(photon.scEta) < 1.5 ? 0 : 3);
    }
};

////////////////////////////////////////////////////////////////////////////////
/// Applies the photon conversion-safe electron veto and (optionally) scale
/// factors.
///
/// This function fetches the scale factor for the ID cut previously applied
/// using applyPhotonID (automatically retrieved from the metainfo).
void applyPhotonCSEV
       (const vector<fs::path>& inputs, //!< input ROOT files (n-tuples)
        const fs::path& output, //!< output ROOT file (n-tuple)
        const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
        const int steering, //!< parameters obtained from explicit options
        const DT::Slice slice = {1,0} //!< number and index of slice
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    unique_ptr<TChain> tIn = DT::GetChain(inputs);
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = unique_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);

    vector<RecPhoton> * recPhotons = nullptr;
    tIn->SetBranchAddress("recPhotons", &recPhotons);

    auto ID = metainfo.Get<string>("corrections", "photons", "ID");
    auto path = config.get<fs::path>("corrections.photons.CSEVtable");
    metainfo.Set<bool>("corrections", "photons", "CSEV", true);

    CSEVApplier applier(path, ID, path != "/dev/null", (steering & DT::syst) == DT::syst);
    for (const auto& name: applier.weightNames())
        metainfo.Set<string>("variations", RecPhoton::WeightVar, name);

    // TODO: ControlPlots

    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        applier(*recPhotons);

        static const bool fillTree = (steering & DT::fill) == DT::fill;
        if (fillTree) tOut->Fill();
    }

    metainfo.Set<bool>("git", "complete", true);
    fOut->cd();
    tOut->Write();

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of namespace DAS::Photon

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Selects photons passing the conversion-safe electron "
                            "veto (CSEV). Photon ID should be applied before.",
                            DT::config | DT::split | DT::fill | DT::syst);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<fs::path>("CSEVtable", "corrections.photons.CSEVtable",
                              "The file from which CSEV scale factors are taken "
                              "(or /dev/null for no correction)");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::Photon::applyPhotonCSEV(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
