#pragma once

#include "Core/Objects/interface/PhysicsObject.h"

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// class GenMuon
struct GenMuon : public PhysicsObject {
    static inline const char * const ScaleVar = "GenMuonScales", //< Name of muon gen scales in variations
                             * const WeightVar = "GenMuonWgts";  //< Name of muon gen weights in variations

    int Q = 0; //!< +/- 1

    GenMuon () = default; //!< Constructor (trivial)
    virtual ~GenMuon() = default; //!< Destructor

private:
    std::string_view scale_group () const override { return ScaleVar; }
    std::string_view weight_group () const override { return WeightVar; }
};

////////////////////////////////////////////////////////////////////////////////
/// class RecMuon
struct RecMuon : public GenMuon {
    static inline const char * const ScaleVar = "RecMuonScales",   //< Name of muon rec scales in variations
                             * const WeightVar = "RecMuonWgts"; //< Name of muon rec weights in variations

    unsigned int selectors; //!< [muon ID & PF isolation](https://github.com/cms-sw/cmssw/blob/CMSSW_9_4_X/DataFormats/MuonReco/interface/Muon.h#L188-L212)

    float Dxy = -999, //!< transverse distance to PV
          Dz = -999; //!< longitudinal distance to PV

    int nTkHits = -1; //!< number of hits in tracker

    RecMuon () = default; //!< Constructor (trivial)

    // TODO: float RecMuon::CorrE (size_t i) const; // preserve mass

private:
    std::string_view scale_group () const final { return ScaleVar; }
    std::string_view weight_group () const final { return WeightVar; }
};

} // end of DAS namespace

inline std::ostream& operator<< (std::ostream& s, const DAS::GenMuon& muon)
{
    return s << muon.p4 << ' ' << muon.Q;
}
inline std::ostream& operator<< (std::ostream& s, const DAS::RecMuon& muon)
{
    return s << muon.p4 << ' ' << muon.Q << ' ' << muon.selectors;
}

#if defined(__ROOTCLING__)
#pragma link C++ class DAS::GenMuon +;
#pragma link C++ class std::vector<DAS::GenMuon> +;

#pragma link C++ class DAS::RecMuon +;
#pragma link C++ class std::vector<DAS::RecMuon> +;
#endif
