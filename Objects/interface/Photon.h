#pragma once

#include "Core/Objects/interface/PhysicsObject.h"

#include <TString.h>

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// class GenPhoton
struct GenPhoton : public PhysicsObject {
    static inline const char * ScaleVar = "GenPhotonScales",   //< Name of photon rec scales in variations
                             * WeightVar = "GenPhotonWgts"; //< Name of photon rec weights in variations

    bool zAncestor = false, //!< Z boson among the particle mothers
         prompt = false; //!< Originates directly from the matrix element

    GenPhoton() = default; //!< Constructor (trivial)

private:
    std::string_view scale_group () const final { return ScaleVar; }
    std::string_view weight_group () const final { return WeightVar; }
};

////////////////////////////////////////////////////////////////////////////////
/// class RecPhoton
struct RecPhoton {
    static inline const char * ScaleVar = "RecPhotonScales",   //< Name of photon rec scales in variations
                             * WeightVar = "RecPhotonWgts"; //< Name of photon rec weights in variations

    /// Energy scale and smearing variations provided by EGamma.
    /// See [TWiki (rev 19)](https://twiki.cern.ch/twiki/bin/view/CMS/EgammaMiniAODV2?rev=19#Energy_Scale_and_Smearing)
    /// and [TWiki (rev 73)](https://twiki.cern.ch/twiki/bin/view/CMS/EgammaUL2016To2018?rev=73#Scale_and_smearing_corrections_f).
    enum EnergyVariation {
        Nominal = 0,    //!< ecalEnergy of photon after scale & smearing corrections
        ScaleUp,        //!< energy with the ecal energy scale shifted 1 sigma(stat) up
        ScaleDown,      //!< energy with the ecal energy scale shifted 1 sigma(stat) down
        SigmaPhiUp,     //!< energy with the ecal energy smearing value shifted 1 sigma(phi) up
        SigmaPhiDown,   //!< energy with the ecal energy smearing value shifted 1 sigma(phi) down
        SigmaRhoUp,     //!< energy with the ecal energy smearing value shifted 1 sigma(rho) up
        SigmaRhoDown,   //!< energy with the ecal energy smearing value shifted 1 sigma(rho) down
        VariationsCount //!< Number of available variations
    };
    static inline const std::vector<TString> uncs {"Scale", "SigmaPhi", "SigmaRho"};

    /// Identification flags provided by EGamma.
    /// See [TWiki (rev 20)](https://twiki.cern.ch/twiki/bin/view/CMS/EgammaRunIIRecommendations?rev=20#Fall17v2_AN1)
    /// Values can be combined as a bit field, e.g. `CutBasedLoose | CutBasedMedium`.
    enum Identification {
        CutBasedLoose              = 0b000001, //!< Loose cut-based ID
        CutBasedMedium             = 0b000010, //!< Medium cut-based ID
        CutBasedTight              = 0b000100, //!< Tight cut-based ID
        MVAWorkingPoint80          = 0b001000, //!< 80% efficiency working point of the MVA ID
        MVAWorkingPoint90          = 0b010000, //!< 90% efficiency working point of the MVA ID
        ConversionSafeElectronVeto = 0b100000, //!< Electron veto
    };

    FourVector p4; //!< four-momentum
    Weights weights = {{1.,0}}; //!< object weights

    float scEta, //!< Super cluster eta, used to veto the barrel/endcap transition region
          sigmaIEtaIEta, //!< Width of the ECAL deposit along the eta axis
          hOverE, //!< Ratio of HCAL to ECAL energy
          chargedIsolation, //!< Recomputed isolation from charged particles
          neutralHadronIsolation, //!< Recomputed isolation from neutral hadrons
          photonIsolation, //!< Recomputed isolation from other photons 
          worstChargedIsolation; //!< Recomputed charged isolation with the vertex chosen to maximize this value used for the ID 

    /// \brief Energy scale and smearing variations, indexed with the \ref EnergyVariation enum.
    /// \see \ref CorrP4
    std::array<float, VariationsCount> scales;
    float ecalEnergyErrPostCorr = -1; //!< resolution estimate on the ecalEnergy after scale & smearing corrections

    std::uint32_t selectors = 0; //!< Identification cuts satisfied by the photon

    RecPhoton () = default; //!< Constructor (trivial)
    ~RecPhoton () = default; //!< Destructor (trivial)

    inline float Rapidity () const { return       p4.Rapidity() ; };
    inline float AbsRap   () const { return std::abs(Rapidity()); };

    /// Obtain the calibrated (data) or smeared (MC) 4-momentum for the given variation
    /// of the corrections.
    inline FourVector CorrP4 (EnergyVariation var = Nominal) const { return p4 * scales[var]; }
};

} // end of DAS namespace

inline std::ostream& operator<< (std::ostream& s, const DAS::GenPhoton& photon)
{
    return s << photon.p4 << ' ' << photon.zAncestor << ' ' << photon.prompt;
}

inline std::ostream& operator<< (std::ostream& s, const DAS::RecPhoton& photon)
{
    return s << photon.p4 << ' ' << photon.scales.size() << ' ' << photon.weights.size();
}

#if defined(__ROOTCLING__)
#pragma link C++ class DAS::GenPhoton +;
#pragma link C++ class std::vector<DAS::GenPhoton> +;

#pragma link C++ class DAS::RecPhoton +;
#pragma link C++ class std::vector<DAS::RecPhoton> +;
#endif
