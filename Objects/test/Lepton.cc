#ifndef DOXYGEN_SHOULD_SKIP_THIS
#include "Core/Objects/interface/Lepton.h"

#define BOOST_TEST_MODULE Lepton
#include <boost/test/included/unit_test.hpp>

#include <iostream>

using namespace DAS;
using namespace std;

BOOST_AUTO_TEST_CASE( genlevel )
{
    GenMuon muon;
    BOOST_TEST( muon.scales.size() == 1 );
    BOOST_TEST( muon.weights.size() == 1 );
    BOOST_TEST( muon.Q == (int) 0 );
    cout << muon << endl;
}

BOOST_AUTO_TEST_CASE( reclevel )
{
    RecMuon muon;
    BOOST_TEST( muon.scales.size() == 1 );
    BOOST_TEST( muon.weights.size() == 1 );
    BOOST_TEST( muon.Q == 0 );
    BOOST_TEST( muon.nTkHits == -1 );
    BOOST_TEST( muon.Dxy == -999 );
    BOOST_TEST( muon.Dz == -999 );
    cout << muon << endl;
}

#endif
