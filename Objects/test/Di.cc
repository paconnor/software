#ifndef DOXYGEN_SHOULD_SKIP_THIS
#include "Core/Objects/interface/Di.h"

#define BOOST_TEST_MODULE Di
#include <boost/test/included/unit_test.hpp>

#include <iostream>
#include <map>

#include <TTree.h>
#include <TFile.h>

using namespace DAS;
using namespace std;

namespace utf = boost::unit_test;

vector<GenJet> GetDummyGenJets ()
{
    GenJet genjet1, genjet2;

    // 4-vectors
    genjet1.p4 = {1,1,1,1};
    genjet2.p4 = {2,2,2,2};

    // JES
    genjet1.scales.push_back(1.5);
    genjet1.scales.push_back(0.5);
    genjet2.scales.push_back(1.5);
    genjet2.scales.push_back(0.5);

    // weights
    genjet1.weights.push_back({1.5,1});
    genjet1.weights.push_back({0.5,1});
    genjet2.weights.push_back({1.5,2});
    genjet2.weights.push_back({0.5,2});

    // collection
    vector<GenJet> genjets;
    genjets.push_back(genjet1);
    genjets.push_back(genjet2);
    return genjets;
}

BOOST_AUTO_TEST_CASE( dijet )
{
    vector<GenJet> * genjets = new vector<GenJet>;
    auto gendijet = new GenDijet;
    bool valid = static_cast<bool>(*gendijet);
    //cout << __LINE__ << ' ' << valid << endl;
    BOOST_TEST(!valid);

    *genjets = GetDummyGenJets();
    cout << *gendijet << endl;
    *gendijet = genjets->at(0) + genjets->at(1);
    valid = static_cast<bool>(*gendijet);
    //cout << *valid << endl;
    BOOST_TEST(valid);

    auto f = make_unique<TFile>("di.root", "RECREATE");

    cout << "Creating tree" << endl;
    {
        auto t = make_unique<TTree>("tree", "tree");
        t->Branch("genJets", &genjets);
        t->Branch("genDijet", &gendijet);
        t->Fill();
        t->Write();
        // NOTE: calling `TTree:Scan()` at this stage leads to a seg
    }

    delete genjets;
    delete gendijet;
    genjets = nullptr;
    gendijet = nullptr;

    cout << "Reading tree" << endl;
    {
        auto t = unique_ptr<TTree>(f->Get<TTree>("tree"));
        t->Show(0);
        cout << "Checking genJets" << endl;
        t->Scan("genJets.CorrPt()");
        cout << "Checking genDijet" << endl;
        t->Scan("genDijet.first.CorrPt():genDijet.second.CorrPt():genDijet.HT()");
        cout << "Setting branch addresses" << endl;
        t->SetBranchAddress("genJets", &genjets);
        t->SetBranchAddress("genDijet", &gendijet);
        t->GetEntry(0);
        BOOST_TEST( genjets->at(0).CorrPt() == 1. );
        BOOST_TEST( genjets->at(1).CorrPt() == 2. );
        BOOST_TEST( gendijet->HT() == 1.5 );
    }
}

BOOST_AUTO_TEST_CASE( methods, * utf::tolerance(0.00001) )
{
    auto genjets = GetDummyGenJets();
    GenDijet gendijet = genjets.at(0) + genjets.at(1);

    BOOST_TEST( gendijet.Rapidity() == 1.3573785791881385 );
    BOOST_TEST( gendijet.AbsRap() == 1.3573785791881385 );

    BOOST_TEST( gendijet.DeltaPhi() == 1.0 );
    BOOST_TEST( gendijet.DeltaR() == 1.4142135623730954 );

    BOOST_TEST( gendijet.Yboost() == 1.2138191136964451 );
    BOOST_TEST( gendijet.Ystar() == -0.45713211039819324 );

    BOOST_TEST( gendijet.Ymax() == 1.6709512240946385 );
    BOOST_TEST( gendijet.HT() == 1.5 );
}

BOOST_AUTO_TEST_CASE( variations )
{
    auto genjets = GetDummyGenJets();
    GenDijet gendijet = genjets.at(0) + genjets.at(1);

    using namespace DAS::Uncertainties;
    cout << genjets.at(0).Weight(nominal) << ' ' << genjets.at(1).Weight(nominal) << ' ' << gendijet.Weight(nominal) << endl;

    map<int, double> bits_results {{1,1.5}, {2,1.5}, {3,1.0}};
    for (auto bit_result: bits_results) {
        int bit = bit_result.first;
        double result = bit_result.second;
        Variation v(GenJet::WeightVar, "wgtUp_stat" + to_string(bit), 1, bit);
        cout << "1st-jet weight: " << genjets.at(0).Weight(v) << '\n'
             << "2nd-jet weight: " << genjets.at(1).Weight(v) << '\n'
             << "dijet weight: " << gendijet.Weight(v) << endl;
        BOOST_TEST( gendijet.Weight(v) == result );
    }

    for (int i = 1; i <= 2; ++i) {
        Variation v(GenJet::ScaleVar, "scale", i);
        cout << "1st-jet corrected pt: " << genjets.at(0).CorrPt(v) << '\n'
             << "2nd-jet corrected pt: " << genjets.at(1).CorrPt(v) << '\n'
             << "dijet corrected pt: " << gendijet.CorrPt(v) << '\n'
             << "dijet corrected HT: " << gendijet.HT(v) << endl;
        BOOST_TEST( genjets.at(0).CorrPt(i) == genjets.at(0).CorrPt(v) );
        BOOST_TEST( genjets.at(1).CorrPt(i) == genjets.at(1).CorrPt(v) );
        BOOST_TEST( 2*gendijet.HT(v) == genjets.at(0).CorrPt(i) + genjets.at(1).CorrPt(i) );
    }
}

#endif
