#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <filesystem>
#include <limits>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"

#include <TString.h>
#include <TFile.h>
#include <TH2D.h>

#include "Math/VectorUtil.h"

#include <darwin.h>
#include <boost/property_tree/info_parser.hpp>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

static const auto feps = numeric_limits<float>::epsilon();

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// Load the two-column file with the HLT and corresponding offline PF thresholds.
vector<double> GetTriggerTurnons (const fs::path& f)
{
    if (!fs::exists(f))
        BOOST_THROW_EXCEPTION(fs::filesystem_error("Bad input", f,
                    make_error_code(errc::no_such_file_or_directory)));

    pt::ptree thresholds;
    read_info(f.string(), thresholds);

    vector<double> edges(1, 30);
    for (const auto& threshold: thresholds)
        edges.push_back(threshold.second.get_value<double>());
    edges.push_back(6500);

    return edges;
}

////////////////////////////////////////////////////////////////////////////////
/// Get MET fraction for each jet trigger separately
void getMETfraction 
              (const vector<fs::path>& inputs, //!< input ROOT file (n-tuple)
               const fs::path& output, //!< output ROOT file (histograms)
               const pt::ptree& config, //!< config handled with `Darwin::Tools::Options`
               const int steering, //!< parameters obtained from explicit options 
               const DT::Slice slice = {1,0} //!< number and index of slice
               )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    unique_ptr<TChain> tIn = DT::GetChain(inputs);
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = unique_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    auto isMC = metainfo.Get<bool>("flags", "isMC");

    RecEvent * rEv = nullptr;
    GenEvent * gEv = nullptr;
    MET * met = nullptr;
    tIn->SetBranchAddress("recEvent", &rEv);
    if (isMC)
        tIn->SetBranchAddress("genEvent", &gEv);
    tIn->SetBranchAddress("met", &met);
    
    vector<RecJet> * recJets = nullptr;
    tIn->SetBranchAddress("recJets", &recJets);

    vector<double> MET_edges;
    for (float edge = 0; edge <= 1; edge += 0.01)
        MET_edges.push_back(edge);
    int nMETbins = MET_edges.size()-1;

    auto turnon_file = config.get<fs::path>("corrections.normalisation.turnons");
    vector<double> trig_edges = GetTriggerTurnons(turnon_file);
    int nTrigBins = trig_edges.size() - 1;

    auto METfraction = new TH2F("METfraction", ";MET fraction;p^{leading}_{T}   (GeV)",
                                    nMETbins, MET_edges.data(),
                                    nTrigBins, trig_edges.data());

    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        if (recJets->empty()) continue;
        static auto rapAcc = y_edges.back();
        if (std::abs(recJets->front().Rapidity()) >= rapAcc) continue;
        if (std::abs(met->SumEt) < feps) continue;

        auto pt = recJets->front().CorrPt();
        auto frac = met->Et/met->SumEt;
        double w = (isMC ? gEv->weights.front().v : 1) * rEv->weights.front().v;

        //cout << setw(15) << pt << setw(15) << frac << setw(15) << w << endl;

        // we do not use the jet weight,
        // because the pt is only determined 
        // to find the right trigger window
        // -> only the event weight matters
        METfraction->Fill(frac, pt, w);
    }

    METfraction->Write();

    metainfo.Set<bool>("git", "complete", true);
    tOut->Write();

    cout << __func__ << ' ' << slice << " end" << endl;
}

} // end of namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Obtain MET fraction per trigger.", DT::split);

        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file"               )
               .arg<fs::path>("turnons", "corrections.normalisation.turnons",
                                         "2-column file with turn-on per trigger");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::getMETfraction(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
