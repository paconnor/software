#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <map>
#include <filesystem>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>

#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TRandom3.h>

#include <darwin.h>

#include "Math/VectorUtil.h"

#include "common.h"
#include "Core/Trigger/interface/match.h"
#include "Core/Normalisation/interface/PhaseSelection.h"

using ROOT::Math::VectorUtil::DeltaR;
using ROOT::Math::VectorUtil::DeltaPhi;

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Normalisation {

vector<double> thresholds;
int nThresholds;

////////////////////////////////////////////////////////////////////////////////
/// `Efficiency` does not exactly contain *the* trigger efficiency curve,
/// but rather the histograms to fill in order to get the efficiency curve,
/// namely the reference trigger and the test trigger histograms.
struct Efficiency {

    const int t;
    const TString suffix; //!< name
    TH2 * ref, //!< count of jets by reference trigger 
        * test, //!< count of jets by test & reference triggers
        * hAll, //<! CP for all matched jets
        * hFired, //!< CP for all matched jets IFF the trigger has fired
        * hFiredThreshold, //!< CP for all matched jets IFF the trigger has fired AND the matched HLT jet is above the HLT threshold
        * hHLTmap; //!< Eta vs phi distribution of the HLT jets

    Efficiency (const char * method, int threshold):
        t(threshold),
        suffix(Form("_%s_%d", method, threshold)),
        ref            (new TH2F("ref"           + suffix, ";p_{T}^{PF corr}   (GeV);N_{ref}"     , nPtBins, pt_edges.data(), nYbins, y_edges.data())),
        test           (new TH2F("test"          + suffix, ";p_{T}^{PF corr}   (GeV);N_{test}"    , nPtBins, pt_edges.data(), nYbins, y_edges.data())),
        hAll           (new TH2F("all"           + suffix, ";p_{T}^{PF corr}   (GeV);p_{T}^{HLT}   (GeV)", nPtBins, pt_edges.data(), nThresholds, thresholds.data())),
        hFired         (new TH2F("fired"         + suffix, ";p_{T}^{PF corr}   (GeV);p_{T}^{HLT}   (GeV)", nPtBins, pt_edges.data(), nThresholds, thresholds.data())),
        hFiredThreshold(new TH2F("firedMatching" + suffix, ";p_{T}^{PF corr}   (GeV);p_{T}^{HLT}   (GeV)", nPtBins, pt_edges.data(), nThresholds, thresholds.data())),
        hHLTmap        (new TH2F("HLTmap"        + suffix, "; #phi^{hlt} ; #eta^{hlt}"            , 200, -M_PI, M_PI, 200, -5, 5))
    {
        assert(find(thresholds.begin(), thresholds.end(), t) != thresholds.end()); // TODO: throw exception
    }

    void Fill (const FourVector& p4, bool fired, double w = 1)
    {
        float y = std::abs(p4.Rapidity());
        ref->Fill(p4.Pt(), y, w);
        if (fired)
            test->Fill(p4.Pt(), y, w);
    }

    void Write (TDirectory * d)
    {
        d->cd();
        auto dd = d->mkdir(Form("HLT%d", t));
        dd->cd();
        for (auto h: {ref, test, hAll, hFired, hFiredThreshold, hHLTmap}) {
            h->SetDirectory(dd);
            TString name = h->GetName();
            name.ReplaceAll(suffix,"");
            h->Write(name);
        }
    }
};

float getPrescale (Trigger * trigger, size_t indx)
{
    float preHLT   = trigger->PreHLT[indx];
    float preL1min = trigger->PreL1min[indx];
    float preL1max = trigger->PreL1max[indx];
    assert(preL1min == preL1max);
    float prescale = preHLT * preL1min;
    return prescale;
}

////////////////////////////////////////////////////////////////////////////////
/// Get trigger curves from the data *n*-tuples
void getTriggerCurves 
                (const vector<fs::path>& inputs, //!< input ROOT file (n-tuple)
                 const fs::path& output, //!< output ROOT file (n-tuple)
                 const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
                 const int steering, //!< parameters obtained from explicit options 
                 const DT::Slice slice = {1,0} //!< number and index of slice
                )
{
    //cout << __func__ << ' ' << slice << " start" << endl; // TODO: fix error

    unique_ptr<TChain> tIn = DT::GetChain(inputs);
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = unique_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);

    const auto usePrescales = config.get<bool>("corrections.normalisation.use_prescales");
    metainfo.Set<bool>("corrections", "normalisation", "use_prescales", usePrescales);

    auto lumi_file = config.get<fs::path>("corrections.normalisation.luminosities");
    pt::ptree triggers_lumi;
    pt::read_info(lumi_file.string(), triggers_lumi);
    for (auto const& trigger_lumi: triggers_lumi)
        thresholds.push_back(stoi(trigger_lumi.first));
    nThresholds = thresholds.size()-1;
    metainfo.Set<fs::path>("corrections", "normalisation", "luminosities", lumi_file);

    Trigger * trigger = nullptr;
    tIn->SetBranchAddress("jetTrigger", &trigger);

    vector<RecJet> * recjets = nullptr;
    vector<FourVector> * hltjets = nullptr;
    tIn->SetBranchAddress("recJets", &recjets);
    tIn->SetBranchAddress("hltJets", &hltjets);

    map<TString, size_t> methods { // methods[name] = shift (first trigger for which the method is used)
        {"emulation", 1}, // default method
        {"emulationShift", 2}, // emulation but with a lower-threshold reference trigger (e.g. trig 40 as reference for trig 80 as test)
        {"TnP", 0} // tag-and-probe method (used in any case of the lowest reference trigger(s)
    };

    TH2 * pt_correlation = new TH2F("ptCorr","; p_{T}^{PF,corr}; p_{T}^{hlt}",nPtBins,pt_edges.data(),nPtBins,pt_edges.data());
    TH1 * Deta = new TH1F("deta","; #Delta #eta; nevt", 200, -5, 5);
    TH1 * Dphi = new TH1F("dphi","; #Delta #phi; nevt", 200, -M_PI, M_PI);
    TH1 * dR   = new TH1F("dR"  ,"; #Delta R; nevt"   , 200, 0, 10);

    map<TString, map<int, Efficiency>> curves;
    for (auto method: methods)
    for (size_t i = method.second; i < thresholds.size(); ++i) {
        int t = thresholds.at(i);
        cout << method.first << '\t' << t << '\n';
        curves[method.first].insert( {t, Efficiency(method.first, t)} );
    }
    cout << flush;

    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        // sanity check
        size_t nbits = trigger->Bit.size();    
        if (nbits < thresholds.size())
            BOOST_THROW_EXCEPTION( DE::AnomalousEvent( Form("nbits = %ld < thresholds.size() = %ld",
                                                             nbits,         thresholds.size()), tIn) );

        // avoid empty events
        if (recjets->size() == 0) continue;

        for (const RecJet& pfjet: *recjets){
            FourVector hltjet = match(pfjet.p4, hltjets);
            if ( hltjet == FourVector()) continue;

            Deta->Fill(pfjet.p4.Eta() - hltjet.Eta(), pfjet.weights.front());
            Dphi->Fill(DeltaPhi(pfjet.p4,hltjet), pfjet.weights.front());
            dR->Fill(DeltaR(pfjet.p4, hltjet), pfjet.weights.front());
            pt_correlation->Fill(pfjet.CorrPt(), hltjet.Pt(), pfjet.weights.front());
        }

        // NOTE: from now on, only the leading jet is considered

        auto leadingInTk = phaseSel(*recjets); // iterator

        if (leadingInTk == recjets->end()) continue;

        FourVector pfjet0 = leadingInTk->p4;
        pfjet0 *= leadingInTk->scales.front();

        FourVector hltjet0 = match(pfjet0, hltjets);
        if ( hltjet0 == FourVector()) continue;

        /***  using emulation method(s) ***/
        for (auto method: methods) {

            if (method.first.Contains("TnP")) continue;
            size_t shift = method.second;

            for (size_t i = shift; i < thresholds.size(); ++i) {

                int t = thresholds[i];
                auto& eff = curves.at(method.first).at(t);

                eff.hAll->Fill(pfjet0.Pt(), hltjet0.Pt());

                // test if previous trigger has fired
                if (!trigger->Bit[i-shift]) continue; 
                eff.hFired->Fill(pfjet0.Pt(), hltjet0.Pt());

                if (hltjet0.Pt() < thresholds.at(i-shift)) continue;
                eff.hFiredThreshold->Fill(pfjet0.Pt(), hltjet0.Pt());

                float wgt = usePrescales ? getPrescale(trigger, i-shift) : 1;
                bool fired = hltjet0.Pt() > t;
                eff.Fill(pfjet0, fired, wgt*recjets->front().weights.front());

                eff.hHLTmap->Fill( pfjet0.Phi(),pfjet0.Eta(),wgt*recjets->front().weights.front() );
            }
        } // end of loop on methods

        /*** using tag & probe method ***/
        for (size_t i = 0; i < thresholds.size(); ++i) {

            int t = thresholds[i];
            auto& eff = curves.at("TnP").at(t);

            eff.hAll->Fill(pfjet0.Pt(), hltjet0.Pt());

            // first, it needs to have fired
            if (!trigger->Bit[i])
                continue;
            eff.hFired->Fill(pfjet0.Pt(), hltjet0.Pt());

            // then we need dijet configurations at PF level:
            // -> 1) at least 2 jets 
            if (recjets->size() == 1) 
                continue;
            // -> 2) back-to-back jets
            if (DeltaPhi(recjets->at(0).p4, recjets->at(1).p4) < 2.4) continue;
            // -> 3) a possible 3rd jet should not be significant
            if (recjets->size() > 2) {
                const double pt0 = recjets->at(0).CorrPt(),
                             pt1 = recjets->at(1).CorrPt(),
                             pt2 = recjets->at(2).CorrPt();
                if (pt2 > 0.15*(pt0 + pt1))
                    continue;
            }

            // we take the probe and the tag randomly
            static TRandom3 Rand(/* seed = */ metainfo.Seed<10989678>(slice));
            double r = Rand.Uniform();
            int itag = (r<0.5),
                iprobe = (r>=0.5);

            FourVector pftag   = recjets->at(itag  ).p4,
                       pfprobe = recjets->at(iprobe).p4;

            float pfprobe_wgt = recjets->at(iprobe).weights.front();
            float pftag_wgt = recjets->at(itag).weights.front();

            pftag   *= recjets->at(itag  ).scales.front();
            pfprobe *= recjets->at(iprobe).scales.front();
            
            // -> 4) selection on the rapidity of the system
            if (std::abs(recjets->at(itag).p4.Eta()) > 1.3) continue;

            // match PF at HLT
            // 1) test if tag can be matched
            //    - if not, continue
            // 2) then test if the probe can be match

            const FourVector& hlttag = match(pftag, hltjets);
            const FourVector& hltprobe = match(pfprobe, hltjets);
            if ( hlttag == FourVector() || hltprobe == FourVector()) continue;
            
            if (hlttag.Pt() > t) {
                eff.hFiredThreshold->Fill(pfjet0.Pt(), hltjet0.Pt());

                const FourVector& hltprobe = match(pfprobe, hltjets);
                bool fired = hltprobe.Pt() > t;

                float wgt = usePrescales ? getPrescale(trigger, i) : 1;
                eff.Fill(pfprobe, fired, wgt * pftag_wgt * pfprobe_wgt);
                eff.hHLTmap->Fill( pfjet0.Phi(),pfjet0.Eta(),wgt*recjets->front().weights.front() );
            }
        }
    }

    cout << "Writing to file" << endl;
    for (auto& method: curves) {
        fOut->cd();
        auto d = fOut->mkdir(method.first);
        for (auto& curve: method.second)
            curve.second.Write(d);
    }
    fOut->cd();
    dR->Write();
    Deta->Write();
    Dphi->Write();
    pt_correlation->Write();

    metainfo.Set<bool>("git", "complete", true);
    fOut->cd();
    tOut->Write();

    //cout << __func__ << ' ' << slice << " end" << endl; // TODO: fix error
}

} // end of DAS::Normalisation namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Determine the components necessary to determine the JetHT trigger efficiency.\n"
                            "Strategy:\n"
                            " - For the first trigger (presumably HLT40), only TnP is available\n"
                            " - For all other triggers, emulation is used by default\n"
                            " - The other methods are used as cross-checks",
                            DT::config | DT::split | DT::fill);

        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<fs::path>("luminosities", "corrections.normalisation.luminosities" , "2-column text file with luminosity per trigger")
               .arg<bool>    ("prescales"   , "corrections.normalisation.use_prescales", "boolean flag, for filling of the histograms");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::Normalisation::getTriggerCurves(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
