#include <TH1.h>
#include <TH2.h>
#include <TDirectory.h>
#include <TString.h>

#include <cassert>

#include "Core/CommonTools/interface/variables.h"
#include "Core/Objects/interface/Event.h"

namespace DAS::PUprofile {

////////////////////////////////////////////////////////////////////////////////
/// Pile-Up plots
///
/// See [TWiki](https://twiki.cern.ch/twiki/bin/viewauth/CMS/PileupJSONFileforData)
/// for explanations on in-time and true pile-up definitions, and how it changes
/// the definition of the calculation mode of `pileupCalc.py`.
struct Plots {

    const TString name;
    TH1F intprofile, //!< in-time pile-up, corresponding to `--calcMode observed` (the number of pileup events for the in-time bunch crossing is selected from a Poisson distribution with a mean equal to the "true" pileup)
         trprofile,  //!< true pile-up, corresponding to `--calcMode true` (average pileup conditions under which the event is generated)
         rho,  //!< soft activity
         nVtx; //!< number of vertices

    Plots (TString Name) :
        name(Name),
        intprofile(Name + "intPU", ";in-time PU;N^{ev}_{eff}", nPUbins, 0, nPUbins),
        trprofile (Name + "trPU" ,    ";true PU;N^{ev}_{eff}", nPUbins, 0, nPUbins),
        rho       (Name + "rho"  ,       ";#rho;N^{ev}_{eff}", nPUbins, 0, nPUbins),
        nVtx      (Name + "nVtx" ,    ";N_{vtx};N^{ev}_{eff}", nPUbins, 0, nPUbins)
    {
        std::cout << __FILE__ << ':' << __func__ << ':' << __LINE__ << '\t' << name << std::endl;
    }

    void operator() 
        (const PileUp& pu,
         float w,
         const char v = '0',
         bool OnTheFly = false)
    {
        trprofile.Fill(pu.GetTrPU(v), w);
        rho .Fill(pu.rho , w);
        nVtx.Fill(pu.nVtx, w);

        // Filling with 100 values of InTime PU to have smoother profile,
        if (OnTheFly==true)
            intprofile.Fill(pu.GetInTimePU(v, OnTheFly), w);
        
        if (OnTheFly==false) {
            const int N = 100;
            w /= N;
            for (int i = 0; i < N; ++i)
                intprofile.Fill(pu.GetInTimePU(v, OnTheFly), w);
        }
    }

    void Write (TDirectory * d)
    {
        assert(d);
        d->cd();
        std::vector<TH1*> hs{&intprofile, &trprofile, &rho, &nVtx};
        for (TH1* h: hs) {
            TString n = h->GetName();
            std::cout << n << std::endl;
            n.ReplaceAll(name,"");
            h->SetDirectory(d);
            h->Write(n);
        }
    }
};

} // end of namespaces
