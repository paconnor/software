#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <map>
#include <filesystem>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/info_parser.hpp>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Format.h"
#include "Core/Objects/interface/Jet.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"

#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TRandom3.h>

#include "Math/VectorUtil.h"

#include "Plots.h"

#include <darwin.h>

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::PUprofile {

////////////////////////////////////////////////////////////////////////////////
/// Get the PU profile from the n-tuple (data and MC)
void getPUprofile
        (const vector<fs::path>& inputs, //!< input ROOT files (n-tuple)
         const fs::path& output, //!< output ROOT file (n-tuple)
         const pt::ptree& config, //!< config handled with `Darwin::Tools::Options`
         const int steering, //!< parameters obtained from explicit options
         const DT::Slice slice = {1,0} //!< number and index of slice
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;
    
    unique_ptr<TChain> tIn = DT::GetChain(inputs);
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = unique_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    //if (!isMC) assert(metainfo.HasCorrection("PUlatest")); // TODO

    auto turnon_file = config.get<fs::path>("corrections.normalisation.turnons");
    if (!fs::exists(turnon_file))
        BOOST_THROW_EXCEPTION( fs::filesystem_error("Can't find turnons",
                turnon_file, make_error_code(errc::no_such_file_or_directory)) );
    pt::ptree turnons;
    pt::read_info(turnon_file.c_str(), turnons);

    RecEvent * recEvt = nullptr;
    GenEvent * genEvt = nullptr;
    PileUp * pileup = nullptr;
    tIn->SetBranchAddress("recEvent", &recEvt);
    if (isMC)
        tIn->SetBranchAddress("genEvent", &genEvt);
    tIn->SetBranchAddress("pileup", &pileup);

    vector<RecJet> * recJets = nullptr;
    tIn->SetBranchAddress("recJets", &recJets);

    TList * ev_wgts = metainfo.List("variations", RecEvent::WeightVar);
    const size_t NEvWgts = max(1,ev_wgts->GetSize()),
                 NVar = isMC ? NEvWgts : NEvWgts + 2; // the variations obtained with PileUp::GetTrPU()
    cout << NEvWgts << ' ' << NVar << endl;

    cout << "Declaring histograms" << endl;

    vector<PUprofile::Plots> global;
    TH2D recptLogWgt("recptLogWgt", ";p_{T}^{rec};log(w)", nPtBins, pt_edges.data(), 400, -30, 20);
    map<int, vector<PUprofile::Plots>> diffs;
    // TODO: also plot without weight
    const vector<string> vars {"nominal", "PU" + SysUp, "PU" + SysDown};
    for (string var: vars) {
        auto name = var + "_global";
        global.push_back(PUprofile::Plots(name));
        for (auto& turnon: turnons) {
            name = var + Form("_HLT_PFJet%d", atoi(turnon.first.c_str()));
            int pt = turnon.second.get_value<int>();
            diffs[pt].push_back(PUprofile::Plots(name));
        }
        name = var + "_ZB";
        diffs[0].push_back(PUprofile::Plots(name));
    }

    PileUp::r3.SetSeed(metainfo.Seed<78923845>(slice));

    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        auto weight = recEvt->weights.front();
        if (isMC) weight *= genEvt->weights.front();

        // find leading pt in tracker acceptance
        // NOTE: this is biased to high-pt analyses
        auto leadingInTk = recJets->begin();
        while (leadingInTk != recJets->end()
                && leadingInTk->AbsRap() >= 3.0) // TODO
            ++leadingInTk;

        auto rit = prev(diffs.rend()); // zero-bias trigger by default
        if (leadingInTk != recJets->end()) { // single jet triggers
            auto leading_pt = leadingInTk->CorrPt();
            
            // find the corresponding trigger
            rit = diffs.rbegin();
            while (rit != diffs.rend()) {
                if (rit->first < leading_pt) break;
                ++rit;
            }
            if (rit == diffs.rend()) { // in case no single-jet trigger has been found, then it should return to ZB case
                cerr << red << "No single-jet trigger has been found with `leading_pt = " << leading_pt 
                     << "` in event " << recEvt->evtNo << ", therefore will consider ZeroBias trigger.\n" << normal;
                rit = prev(diffs.rend());
            }
        }
        
        // fill the plots
        global.at(0)(*pileup, weight, '0', isMC);
        global.at(1)(*pileup, weight, '+', isMC);
        global.at(2)(*pileup, weight, '-', isMC);
        auto& diff = rit->second;
        diff  .at(0)(*pileup, weight, '0', isMC);
        diff  .at(1)(*pileup, weight, '+', isMC);
        diff  .at(2)(*pileup, weight, '-', isMC);

        
        if (recJets->size() > 0)
            recptLogWgt.Fill(recJets->front().CorrPt(), log(weight));
    }

    cout << "Writing" << endl;
    for (size_t i = 0; i < 3; ++i) {
        fOut->cd();
        auto var = vars.at(i);
        auto d = fOut->mkdir(var.c_str());
        global.at(i).Write(d);
        for (auto& diff: diffs) {
            TString name(diff.second.at(i).name);
            name.ReplaceAll(var + "_", "");
            auto dd = d->mkdir(name);
            diff.second.at(i).Write(dd);
        }
    }

    fOut->cd();
    recptLogWgt.Write("recptLogWgt");
    tOut->Write();

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of DAS::PUprofile namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Produces the PU profiles from the events stored in the selection.", DT::config | DT::split);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<fs::path>("turnons", "corrections.normalisation.turnons", "2-column file with HLT and offline-PF thresholds");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::PUprofile::getPUprofile(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
