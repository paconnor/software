#include <cassert>
#include <limits>
#include <vector>
#include <filesystem>
#include <memory>

#include <TString.h>
#include <TFile.h>
#include <TH2.h>

#include "exceptions.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/Objects/interface/Jet.h"

static const auto feps = std::numeric_limits<float>::epsilon();

namespace DAS::JetVeto {

////////////////////////////////////////////////////////////////////////////////
/// Check jets reconstructed in hot regions of the detector and give them a 
/// weight of zero (previously, they were just removed from the list, which is 
/// fine for inclusive jet and b jet analyses, but which screws up multi-jet
/// analyses).
///
/// We apply a conservative apporach in this code:
/// one could refine a bit the code to apply it including time
/// dependence (with a weight representative of the luminosity of the
/// era), but the effect being very fine for 2016 and only in the
/// tracker acceptance, we have not considered it 
struct Conservative {

    std::unique_ptr<TH2> h; //!< jet veto map

    Conservative (const std::filesystem::path& jetveto_file)
    {
        using namespace std;
        namespace fs = filesystem;
        namespace DE = Darwin::Exceptions;
        if (!fs::exists(jetveto_file))
            BOOST_THROW_EXCEPTION( fs::filesystem_error("No such file.", jetveto_file,
                                   make_error_code(errc::no_such_file_or_directory)) );

        auto f = make_unique<TFile>(jetveto_file.c_str(), "READ");
        h = unique_ptr<TH2>(f->Get<TH2>("h2hot2"));
        if (!h) BOOST_THROW_EXCEPTION( DE::BadInput("Veto maps can't be found in input ROOT file.", f) );

        if (h->Integral() < feps)
            BOOST_THROW_EXCEPTION( DE::BadInput("Empty histogram.", h) );

        for (int x = 1; x <= h->GetNbinsX(); ++x)
        for (int y = 1; y <= h->GetNbinsY(); ++y) {
            float eff = h->GetBinContent(x,y);
            if (0 <= eff && eff <= 1) continue;
            BOOST_THROW_EXCEPTION( DE::BadInput("The content of the histogram does "
                                        "not seem to correspond to efficiencies.", h) );
        }
    }

    void operator() (DAS::RecJet& jet)
    {
        int x = h->GetXaxis()->FindBin(jet.p4.Eta()),
            y = h->GetYaxis()->FindBin(jet.p4.Phi());
        float eff = h->GetBinContent(x,y);
        jet.weights *= eff;
    }

    void Write (TDirectory * dir)
    {
        dir->cd();
        h->SetDirectory(dir);
        h->Write("jetvetomap");
    }

};

}
