#!/bin/zsh
set -e

# counter
N=0

cd $CMSSW_BASE/bin/$SCRAM_ARCH/
for i in $(grep -rIL .)
do
    echo "\x1B[1m$i\x1B[0m"
    out=$($i -h | grep "\-e \[ --example \]          Print config example" || test $? = 1)
    if [[ -n $out ]]
    then
        ./$i -e
        ((N=N+1))
    fi
done

# this is a safety in case the grep no longer works bc of a change in ProtoDarwin
[[ N -gt 0 ]]
