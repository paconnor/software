#ifndef parameters_h
#define parameters_h

// std
#include <vector>
#include <string>
#include <set>
#include <filesystem>

// CMSSW framework
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Framework/interface/ConsumesCollector.h"

// particles
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"

// reco
#include "DataFormats/JetReco/interface/GenJetCollection.h"
#include "DataFormats/JetReco/interface/JetCollection.h"
#include "DataFormats/MuonReco/interface/Muon.h"
#include "DataFormats/VertexReco/interface/Vertex.h"

// PAT
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Photon.h"
#include "DataFormats/PatCandidates/interface/MET.h"

// trigger
#include "DataFormats/PatCandidates/interface/PackedTriggerPrescales.h"
#include "DataFormats/PatCandidates/interface/TriggerObjectStandAlone.h"
#include "HLTrigger/HLTcore/interface/HLTConfigProvider.h"

// flavour
#include "SimDataFormats/GeneratorProducts/interface/GenEventInfoProduct.h"
#include "DataFormats/JetMatching/interface/JetFlavourInfo.h"
#include "DataFormats/JetMatching/interface/JetFlavourInfoMatching.h"
#include "DataFormats/Candidate/interface/VertexCompositePtrCandidateFwd.h"

// pile up
#include "SimDataFormats/PileupSummaryInfo/interface/PileupSummaryInfo.h"

// model
#include "SimDataFormats/GeneratorProducts/interface/LHEEventProduct.h"
#include "SimDataFormats/GeneratorProducts/interface/LHERunInfoProduct.h"

// boost
#include <boost/property_tree/ptree.hpp>
#include <boost/optional.hpp>

namespace DAS {

struct Parameters {

    // event information
    const bool isMC; //!< flag
    const int year; //!< 20xx
    const bool sandbox; //!< flag for CRAB sandbox
    static boost::property_tree::ptree ReadJSON (std::filesystem::path, bool = false);
    boost::property_tree::ptree config; //!< input JSON config
    const boost::property_tree::ptree& options; //!< booleans to steer branch filling
    edm::EDGetTokenT<GenEventInfoProduct> genEvtInfoToken;
    edm::EDGetTokenT<reco::GenParticleCollection> genParticlesToken;

    // jets
    edm::EDGetTokenT<reco::GenJetCollection> genjetsToken;
    edm::EDGetTokenT<pat::JetCollection> recjetsToken;
    const bool jets, PUjetID, flavour;
    edm::EDGetTokenT<reco::JetFlavourInfoMatchingCollection> jetFlavourInfosToken;
    edm::EDGetTokenT<std::vector<reco::VertexCompositePtrCandidate>> secVertexInfoToken;

    // muons
    const bool muons;
    edm::EDGetTokenT<edm::View<reco::Candidate>> genLeptonsToken;
    edm::EDGetTokenT<pat::MuonCollection> recmuonsToken;

    // photons
    const bool photons;
    edm::EDGetTokenT<pat::PhotonCollection> recphotonsToken;

    // MET & trigger
    const bool triggers;
    edm::EDGetTokenT<pat::METCollection> metToken;
    edm::EDGetTokenT<edm::TriggerResults> metResultsToken, triggerResultsToken;
    edm::EDGetTokenT<reco::VertexCollection> recVtxsToken;
    edm::EDGetTokenT<pat::PackedTriggerPrescales> triggerPrescalesToken, triggerPrescalesl1minToken, triggerPrescalesl1maxToken;
    std::vector<std::string> triggerNames_, metNames_;
    edm::EDGetTokenT<pat::TriggerObjectStandAloneCollection> triggerObjectsToken;
    std::set<std::string> HLTjet_triggerNames;

    // pile-up
    edm::EDGetTokenT<double> rhoToken;
    edm::EDGetTokenT<std::vector<PileupSummaryInfo> > pileupInfoToken;

#ifdef PS_WEIGHTS
    // model
    edm::EDGetTokenT<LHEEventProduct> lheToken;
#endif

    // constructor
    Parameters (edm::ParameterSet const& cfg, edm::ConsumesCollector && iC);

    std::set<std::string> getHLTjet_triggerNames (const std::vector<std::string>&);
};

} // end of DAS namespace

#endif
