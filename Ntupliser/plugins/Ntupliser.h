#pragma once

#include "FWCore/Framework/interface/EDAnalyzer.h"
#include "FWCore/Framework/interface/EventSetup.h"
#include "FWCore/Framework/interface/ESHandle.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "FWCore/Common/interface/TriggerNames.h"

#include "CommonTools/UtilAlgos/interface/TFileService.h"

#include "SimDataFormats/GeneratorProducts/interface/GenEventInfoProduct.h"
#include "SimDataFormats/PileupSummaryInfo/interface/PileupSummaryInfo.h"

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Lepton.h"
#include "Core/Objects/interface/Event.h"

#include "Parameters.h"
#include "helper.h"

#include "TTree.h"
#include "TH1F.h"

#include <darwin.h>

#include <boost/property_tree/ptree.hpp>

class Ntupliser : public edm::EDAnalyzer
{
public:
    typedef reco::Particle::LorentzVector LorentzVector;

    // default method in EDAnalyzer
    explicit Ntupliser(edm::ParameterSet const& cfg);
    virtual void beginJob() override;
    virtual void beginRun(edm::Run const& iRun, edm::EventSetup const& iSetup) override;
    virtual void endRun(edm::Run const& iRun, edm::EventSetup const& iSetup) override;
    virtual void analyze(edm::Event const& iEvent, edm::EventSetup const& iSetup) override;
    virtual void endJob() override;
    virtual ~Ntupliser() override;

private:

    void reset ();
    void initialise (edm::Event const& iEvent);
    bool trigger (edm::Event const& iEvent);
    void fillMET (edm::Event const& iEvent);
    void getHLTjets (edm::Event const& iEvent);
    template<typename MyJetCollection> void getGenJets (edm::Handle<MyJetCollection>& mygenjets);
    void getRecJets ();
    void getGenMuons ();
    void getRecMuons ();
    void getGenPhotons ();
    void getRecPhotons ();
    void getSecVertices ();
    void getEventVariables (edm::Event const& iEvent);

    // configurable parameters
    DAS::Parameters p;
    DAS::Helper h;

    // input objects
    edm::Handle<reco::GenJetCollection> genjets;
    edm::Handle<pat::JetCollection> recjets;
    edm::Handle<edm::View<reco::Candidate>> genLeptons;
    edm::Handle<pat::MuonCollection> recmuons;
    edm::Handle<pat::PhotonCollection> recphotons;
    edm::Handle<pat::METCollection> met;
    edm::Handle<double> rho;
    edm::Handle<reco::VertexCollection> recVtxs;
    edm::Handle<edm::TriggerResults> triggerResults, metResults;
    edm::Handle<pat::PackedTriggerPrescales> triggerPrescales,triggerPrescalesl1min, triggerPrescalesl1max;
    edm::Handle<pat::TriggerObjectStandAloneCollection> triggerObjects;
    edm::Handle<GenEventInfoProduct> genEvtInfo;
    edm::Handle<reco::GenParticleCollection> genParticles;
    edm::Handle<reco::JetFlavourInfoMatchingCollection> theJetFlavourInfos;
    edm::Handle<std::vector<reco::VertexCompositePtrCandidate>> SVs;
    edm::Handle<std::vector<PileupSummaryInfo>> pileupInfo;
#ifdef PS_WEIGHTS
    edm::Handle<LHEEventProduct> lhe;
#endif

    // output file
    edm::Service<TFileService> fs_;
    TTree *tree;
    Darwin::Tools::MetaInfo metainfo;

    // output objects
    std::vector<DAS::RecJet> *recJets_;
    std::vector<DAS::FourVector> *HLTjets_;
    std::vector<DAS::GenJet> *genJets_;
    std::vector<DAS::RecMuon> *recMuons_;
    std::vector<DAS::GenMuon> *genMuons_;
    std::vector<DAS::RecPhoton> *recPhotons_;
    std::vector<DAS::GenPhoton> *genPhotons_;
    DAS::Trigger *jetTrigger_, *muonTrigger_;
    DAS::GenEvent *genEvent_;
    DAS::RecEvent *recEvent_;
    DAS::MET *met_;
    DAS::PileUp *pileup_;
    DAS::PrimaryVertex *primaryvertex_;

    boost::property_tree::ptree pileup_json; //!< pileup "latest"
};
