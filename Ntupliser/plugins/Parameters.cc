#include "Parameters.h"

#include <iostream>

#include <boost/property_tree/json_parser.hpp>

namespace pt = boost::property_tree;

using namespace edm;
using namespace reco;
using namespace pat;
using namespace std;

namespace fs = filesystem;

#define gP cfg.getParameter
#define iCc iC.consumes

set<string> DAS::Parameters::getHLTjet_triggerNames (const vector<string>& triggerNames)
{
    set<string> HLTjet_triggerNames;
    for (const string& triggerName: triggerNames)
        if (triggerName.find("PFJet"))
            HLTjet_triggerNames.insert(triggerName);
    return HLTjet_triggerNames;
}

pt::ptree DAS::Parameters::ReadJSON (fs::path fname, bool sandbox)
{
    pt::ptree config;
    if (sandbox) fname = fname.filename();
    read_json(fname.string(), config);
    return config;
}

bool find_by_value (const pt::ptree& options, const string& value)
{
    for (const auto& o: options)
        if (o.second.get_value<string>() == value)
            return true;
    return false;
}

DAS::Parameters::Parameters(ParameterSet const& cfg,
                       ConsumesCollector && iC) :
    isMC(gP<bool>("isMC")),
    year(gP<int>("year")),
    sandbox(gP<bool>("sandbox")),
    config(ReadJSON(gP<string>("config"), sandbox)),
    options(config.get_child("flags.options")),
    jets    (find_by_value(options, "jets"    )),
    PUjetID (gP<bool>("PUjetID")),
    flavour (find_by_value(options, "flavour" )),
    muons   (find_by_value(options, "muons"   )),
    photons (find_by_value(options, "photons" )),
    triggers(find_by_value(options, "triggers"))
{
    // options
    config.put<bool>("flags.isMC", isMC);
    config.put<int>("flags.year", year);

    // jets
    if (isMC) {
        genEvtInfoToken = iCc<GenEventInfoProduct>(InputTag("generator"));
        genjetsToken    = iCc<GenJetCollection>(gP<InputTag>("genjets"));
        if (flavour)
            jetFlavourInfosToken = iCc<JetFlavourInfoMatchingCollection>( gP<InputTag>("jetFlavourInfos"));
        genParticlesToken = iCc<GenParticleCollection>(gP<InputTag>("genparticles"));
    }
    recjetsToken = iCc<JetCollection>(gP<InputTag>("recjets"));

    // muons
    if (muons) {
        if (isMC)
            genLeptonsToken = iCc<edm::View<reco::Candidate>>(gP<InputTag>("genLeptons"));
        recmuonsToken = iCc<pat::MuonCollection>(gP<InputTag>("recmuons")); // need to specify the namespace to avoid conflict
    }

    // photons
    if (photons)
        recphotonsToken = iCc<pat::PhotonCollection>(gP<InputTag>("recphotons")); // need to specify the namespace to avoid conflict

    // pile-up
    rhoToken        = iCc<double>(gP<InputTag>("rho"));
    recVtxsToken    = iCc<VertexCollection>(gP<InputTag>("vertices"));
    pileupInfoToken = iCc<vector<PileupSummaryInfo>>(cfg.getUntrackedParameter<edm::InputTag>("pileupInfo"));

    // trigger
    if (triggers) {
        triggerNames_         = gP<vector<string>>("triggerNames");
        triggerResultsToken   = iCc<TriggerResults>(gP<InputTag>("triggerResults"));
        triggerPrescalesToken = iCc<PackedTriggerPrescales>(gP<InputTag>("triggerPrescales"));
        triggerPrescalesl1minToken = iCc<PackedTriggerPrescales>(gP<InputTag>("triggerPrescalesl1min"));
        triggerPrescalesl1maxToken = iCc<PackedTriggerPrescales>(gP<InputTag>("triggerPrescalesl1max"));
        triggerObjectsToken   = iCc<TriggerObjectStandAloneCollection>(gP<InputTag>("triggerObjects"));
        HLTjet_triggerNames = getHLTjet_triggerNames(triggerNames_);
    }

    // MET
    metToken        = iCc<METCollection>(gP<InputTag>("met"));
    metNames_       = gP<vector<string>>("metNames");
    metResultsToken = iCc<TriggerResults>(gP<InputTag>("metResults"));

#ifdef PS_WEIGHTS
    // model
    if (isMC)
        lheToken = iCc<LHEEventProduct,edm::InEvent>(gP<edm::InputTag>("lhe"));
#endif
}
