# RivetNtupliser

After setting up the environment (and defining `RIVET_ANALYSIS_PATH` as `/absolutepath/to/Ntupliser/src`) and doing `scram b`, the ntupliser can be run with the command:

```
cmsRun /path/to/Ntupliser/python/Rivet_cfg.py configFile=/path/to/Ntupliser/test/Rivet.json
```

The command above a TTree file, `output.root`, containing the desired branches in the in your current working directory.

It is also possible to set the name of the desired ouptut file in the command line:

```
cmsRun /path/to/Ntupliser/python/Rivet_cfg.py configFile=/path/to/Ntupliser/test/Rivet.json outputFile=some_output.root
```

`Rivet.json` is an example configuration file which contains configuration parameters for the desired MC jet events.
Available Configurations:

- "maxEvents": the number of events wished to be simulated.

- "JetRadius": The R parameter for the jets you wish to simulate, which are clustered with the anti-kt algorithm by default. Jet radius can be a single value or a list of multiple values (e.g. JetRadius : [0.4, 0.8]. 

- "generator": MC generator with CMSSW compatibility (see https://github.com/cms-sw/cmssw/tree/master/Configuration/Generator/python). Default generator is `Pythia8GeneratorFilter`.

- "detector": boolean that simulates `SmearedJets` and records them as `recJets` branch in the `events` tree.

- "selection": phase space selections in the desired sample, e.g.
```
    "selection": {
        "AbsEtaMin":0.0,
        "AbsEtaMax":10.0,
        "pTJetMin":0.0,
        "pTJetMax":5000
                }
```

- "physics": configurations relating to the generator simulation physics, e.g. 

```
    "physics": {
        "MPI": "on",
        "HAD":"on" }
```

where the "MPI" and "HAD" flags set the pythia8 parameters `PartonLevel:MPI` and `HadronLevel:all`, respectively.
