#include <cassert>
#include <algorithm>
#include <iostream>
#include <vector>

#include <TF1.h>
#include <TH1.h>
#include <TDirectory.h>

namespace DAS {

using namespace std;

TH1 * SaveTF1 (TDirectory * d, TF1 * f)
{
    assert(f != nullptr);

    const char * name = Form("p_%s", f->GetName());
    const int N = f->GetNpar();
    TH1 * h = new TH1D(name, f->GetTitle(), N, 0.5, N+0.5);
    double m, M;
    f->GetRange(m,M);
    h->SetBinContent(0, m);
    h->SetBinContent(N+1, M);
    for (int i = 0; i < N; ++i) {
        double p = f->GetParameter(i),
               e = f->GetParError (i);
        h->SetBinContent(i+1,p);
        h->SetBinError  (i+1,e);
    }
    h->SetDirectory(d);
    h->Write();
    return h;
}

template<typename Functor> TF1 * GetTF1 (TH1 * h)
{
    assert(h != nullptr);
    double m = h->GetBinContent(0),
           N = h->GetNbinsX(),
           M = h->GetBinContent(N+1);
    const char * name = h->GetName();
    Functor functor(m, M);
    TF1 * f = new TF1(&name[2], functor, m, M, N); // assume "p_" in the name of the histogram
    cout << f->GetName() << endl;
    for (int i = 0; i < N; ++i) {
        double p = h->GetBinContent(i+1),
               e = h->GetBinError  (i+1);
        f->SetParameter(i, p);
        f->SetParError (i, e);
    }
    return f;
}

template<typename Functor> TF1 * GetTF1 (TDirectory * d, const char * location)
{
    assert(d != nullptr);
    TH1 * h = dynamic_cast<TH1*>(d->Get(location));
    return GetTF1<Functor>(h);
}

}
