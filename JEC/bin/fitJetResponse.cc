#include <cstdlib>
#include <cmath>
#include <cassert>
#include <iostream>
#include <map>
#include <filesystem>

#include <TString.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TF1.h>
#include <TKey.h>

#include "Core/JEC/interface/resolution.h"
#include "Core/JEC/interface/DoubleCrystalBall.h"

#include "common.h"

#include <darwin.h>

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::JetEnergy {

////////////////////////////////////////////////////////////////////////////////
/// Find
/// either: Maximum of log(d) at LHS corresponds to left transition point
/// or    : Minimum of log(d) at RHS corresponds to right transition point
int findMaxMinBin_logd (double X,
                        TH1 * h,
                        TH1 * logd,
                        bool findMax) //!< true for max, false for min
{
    int binX   = h->FindBin(X),
        binmax = h->GetMaximumBin(),
        binx   = binX,
        tempCont = 0, // The actual Max or Min content
        iStart = findMax ? binX   : binmax+1,
        iStop  = findMax ? binmax : binX;
    for (int i = iStart; i < iStop; ++i) {
        double content = logd->GetBinContent(i);
        if (findMax) { if (tempCont > content) continue; }
        else         { if (tempCont < content) continue; }
        tempCont = content;
        binx = i;
    }
    return binx;
}

////////////////////////////////////////////////////////////////////////////////
/// Projection 3D histograms onto 1D histograms and fit response
///
/// Tuning (1): For high pT bins that we have less statistics we choose to Rebin less.
///             Seems to give better fit results.
void FitResponse1D (TDirectory * dir,
                    TH2 * res2D)
{
    dir->cd();

    TH1 * h_ptBins = res2D->ProjectionX("h_ptBins", 1, -1); // Only for pt binnning extraction
    h_ptBins->GetXaxis()->SetTitle("p_{T}^{gen}");

    // Declaring histograms (control plots)
    map<const char *, TH1 *> hists;
    for (const char * name: {"meanCore", "sigmaCore", // Obtained from DCB fit
                             "kLtail", "kRtail"}) { // DCB tails
        hists[name] = (TH1*) h_ptBins->Clone(name);
        hists[name]->Reset();
        hists[name]->SetTitle(name);
        hists[name]->SetDirectory(0);
    }
    delete h_ptBins;

    int Npt     = res2D->GetXaxis()->GetNbins(),
        lastBin = res2D->GetXaxis()->GetNbins();
    for (int ptbin = Npt; ptbin >= 1; --ptbin) { // Deduce bin after which all other bins will be skipped
        TH1 * h_temp = res2D->ProjectionY("h_temp", ptbin, ptbin);
        if (h_temp->GetEntries() > 100) {
            lastBin = ptbin;
            break;
        }
    }

    for (int ptbin = 1; ptbin <= Npt; ++ptbin) { // Repeat fit procedure for all response distributions
        double lowEdge = res2D->GetXaxis()->GetBinLowEdge(ptbin),
               upEdge  = res2D->GetXaxis()->GetBinUpEdge (ptbin);
        const char * level = TString( res2D->GetXaxis()->GetTitle() ).Contains("gen") ? "gen" : "rec";
        TString title    = Form("%.0f < p_{T}^{%s} < %.0f", lowEdge, level, upEdge),
                stdPrint = TString( res2D->GetTitle() );
        stdPrint.ReplaceAll("zx projection", "");
        stdPrint.ReplaceAll(")", "");
        stdPrint += ", " + title + ")";
        cout << "---------- " << orange << stdPrint << normal << endl;

        TH1 * h = res2D->ProjectionY("h", ptbin, ptbin); // Response distribution
        h->GetXaxis()->SetTitle("#frac{p_{T}^{rec}}{p_{T}^{gen}}");
        if (h->GetEntries() < 100) {
            cout << "-- Skipped due to low number of events!" << endl;
            continue;
        }

        if (ptbin <= lastBin-3) h->Rebin(4); // Tuning (1)
        else                    h->Rebin(2);

        float normalisation = abs( h->Integral("width") );
        if (normalisation < feps) {
            cout << "-- Skipped!" << endl;
            continue;
        }

        TDirectory * d_pt = dir->mkdir(Form("ptbin%d", ptbin), title);
        d_pt->cd();
        h->SetTitle(title);
        h->SetDirectory(d_pt);
        h->Scale(1.f/normalisation);
        h->Write();

        // Calculate 2nd derivative of the log(h)
        TH1 * logd  = DerivativeFivePointStencil(h, safelog), // 1st derivative of log(h)
            * logdd = DerivativeFivePointStencil(logd      ); // 2nd derivative of log(h)
        logd->Write("logd");
        logdd->Write("logdd");

        // Preparing for fit, setting ranges
        // Assuming response is centered around 1
        int binl = findMaxMinBin_logd(0.2, h, logd,  true), // Find Max
            binr = findMaxMinBin_logd(1.7, h, logd, false); // Find Min
        float l = h->GetXaxis()->GetBinUpEdge(binl), // [l,r] Gaussian core range assumption
              r = h->GetXaxis()->GetBinLowEdge(binr);

        auto f = new TF1("f", DAS::DoubleCrystalBall::Distribution(), l, r, 7); // Starting by passing Gaussian range
        double p[7]  = {1.   , h->GetMean()     , h->GetStdDev()     , l-0.3, 3 , r+0.3, 3 }, // Initial DCB paramateres
               pe[7] = {0.001, h->GetMeanError(), h->GetStdDevError(),   0.1, 1.,   0.1, 1.}; // Parameter errors

        // Gaussian core fit, first estimation of mu and sigma
        f->SetTitle(title);
        f->SetParameters(p);
        f->SetParErrors(pe);
        for (int i = 3; i <= 6; ++i)
            f->FixParameter(i, p[i]);
        cout << "-- Fitting core between " << l << ' ' << r << endl;
        TFitResultPtr result = h->Fit(f, "QS0NRE");
        printf("-- Gaussian Fit        : %.3f %.3f %.3f %.3f %.3f %.3f %.3f \n", f->GetParameter(0), f->GetParameter(1), f->GetParameter(2),
                                                                                 f->GetParameter(3), f->GetParameter(4), f->GetParameter(5), f->GetParameter(6));
        printf("--\n");
        f->Write("gausCore");

        double mu    = f->GetParameter(1),
               sigma = f->GetParameter(2);

        // DCB fit, estimate of LHS parameters
        f->FixParameter(0,1);
        f->SetParLimits(1,  mu-sigma, mu+sigma);
        f->SetParLimits(2, 0.5*sigma,  2*sigma);

        int binL = h->FindBin(mu-3*sigma);
        double L = h->GetXaxis()->GetBinUpEdge(binL); // [L,r] DCB left range assumption
        cout << "-- Fitting LHS CB between " << L << ' ' << r << endl;
        f->SetRange(L, r);
        f->GetParameters(p);
        f->ReleaseParameter(3);
        f->ReleaseParameter(4);
        f->SetParLimits(3, mu-3*sigma, mu-sigma);
        f->SetParameter(3, l);
        f->SetParLimits(4, 1.001,  40);
        f->SetParameter(4, 3);
        result = h->Fit(f, "QS0NRE");
        printf("-- Single-sided CB Fit : %.3f %.3f %.3f %.3f %.3f %.3f %.3f \n", f->GetParameter(0), f->GetParameter(1), f->GetParameter(2),
                                                                                 f->GetParameter(3), f->GetParameter(4), f->GetParameter(5), f->GetParameter(6));
        printf("--\n");
        f->Write("SSCB");

        // DCB fit, estimate of RHS parameters
        int binR = h->FindBin(mu+3*sigma);
        double R = h->GetXaxis()->GetBinLowEdge(binR); // [L,R] DCB right range assumption
        cout << "-- Fitting LHS and RHS DCB between " << L << ' ' << R << endl;
        f->SetRange(L, R);
        f->ReleaseParameter(5);
        f->ReleaseParameter(6);
        f->SetParLimits(5, mu+sigma, mu+3*sigma);
        f->SetParameter(5, r);
        f->SetParLimits(6, 1.001, 40);
        f->SetParameter(6, 5);
        result = h->Fit(f, "QS0NRE");
        printf("-- Double-sided CB Fit : %.3f %.3f %.3f %.3f %.3f %.3f %.3f \n", f->GetParameter(0), f->GetParameter(1), f->GetParameter(2),
                                                                                 f->GetParameter(3), f->GetParameter(4), f->GetParameter(5), f->GetParameter(6));
        f->Write("DSCB");

        // Save fit parameters into control plots
        mu = f->GetParameter(1);
        assert(isfinite(mu));
        sigma = f->GetParameter(2);
        assert(isfinite(sigma));

        double kL = f->GetParameter(3);
        assert(isfinite(kL));

        double kR = f->GetParameter(5);
        assert(isfinite(kR));

        hists.at( "meanCore")->SetBinContent(ptbin, f->GetParameter(1));
        hists.at( "meanCore")->SetBinError  (ptbin, f->GetParError (1));
        hists.at("sigmaCore")->SetBinContent(ptbin, f->GetParameter(2));
        hists.at("sigmaCore")->SetBinError  (ptbin, f->GetParError (2));

        hists.at("kLtail")->SetBinContent(ptbin, f->GetParameter(3));
        hists.at("kLtail")->SetBinError  (ptbin, f->GetParError (3));
        hists.at("kRtail")->SetBinContent(ptbin, f->GetParameter(5));
        hists.at("kRtail")->SetBinError  (ptbin, f->GetParError (5));

        delete h;
    }

    dir->cd();
    TString title = res2D->GetTitle();
    title.ReplaceAll("Response distribution", "");
    title.ReplaceAll("zx projection", "");
    for (auto h: hists) {
        h.second->SetDirectory(dir);
        h.second->SetTitle(h.second->GetTitle() + title);
        h.second->Write();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// Copy directory structure.
/// Given an input file dIn recursively loop over all directory
/// hierarchy/contents and reproduce it in output file dOut.
/// For each TH3 instance found containing the response
/// perform a DCB fit by calling FitResponse1D() function.
void loopDirsFromGetResponse (TDirectory * dIn,
                              TDirectory * dOut)
{
    for (const auto&& obj: *(dIn->GetListOfKeys())) {
        auto const key = dynamic_cast<TKey*>(obj);
        if ( key->IsFolder() ) {
            auto ddIn = dynamic_cast<TDirectory*>( key->ReadObj() );
            if (ddIn == nullptr) continue;
            TDirectory * ddOut = dOut->mkdir(ddIn->GetName(), ddIn->GetTitle());
            ddOut->cd();
            cout << "Creating directory: " << bold << ddIn->GetPath() << normal << endl;

            loopDirsFromGetResponse(ddIn, ddOut);
        }
        else if ( TString( key->ReadObj()->ClassName() ).Contains("TH3") ) {
            // Assuming for axis, x: pt bins, y: eta bins, z: resolution bins
            auto res3D = dynamic_cast<TH3*>( key->ReadObj() );
            res3D->SetDirectory(0);
            bool isAbsEta = res3D->GetYaxis()->GetBinLowEdge(1) >= 0;
            TDirectory * ddOut = dOut->mkdir(res3D->GetName(), res3D->GetTitle());
            for (int etabin = 1; etabin <= res3D->GetNbinsY(); ++etabin) {
                float lowedge = res3D->GetYaxis()->GetBinLowEdge(etabin),
                       upedge = res3D->GetYaxis()->GetBinUpEdge (etabin);
                TString title = isAbsEta ? Form("%.3f < |#eta| < %.3f", lowedge, upedge)
                                         : Form("%.3f < #eta < %.3f"  , lowedge, upedge);
                res3D->GetYaxis()->SetRange(etabin, etabin);
                auto res2D = dynamic_cast<TH2*>( res3D->Project3D("ZX") ); // Z vs X, where Z is vertical and X is horizontal axis
                res2D->SetTitle( TString( res2D->GetTitle() ).ReplaceAll(") ", ", " + title + ")") );
                ddOut->cd();
                TDirectory * dddOut = ddOut->mkdir(Form("etabin%d", etabin), title);
                cout << "Creating directory: " << bold << dddOut->GetPath() << normal << endl;

                // Fit response distributions
                FitResponse1D(dddOut, res2D);
            }
        }
        else cout << "Found " << orange << key->ReadObj()->GetName() << normal << " of `"
                    << underline << key->ReadObj()->ClassName() << normal << "` type: ignoring it" << endl;
    } // End of for (list of keys) loop
}

////////////////////////////////////////////////////////////////////////////////
/// Fit response distributions from `getJetResponse`
void fitJetResponse
            (const fs::path& input, //!< input ROOT file (histograms)
             const fs::path& output, //!< name of output ROOT (histograms)
             const pt::ptree& config, //!< config file from `DT::Options`
             const int steering //!< steering parameters from `DT::Options`
            )
{
    // TODO: use config and steering (e.g. for verbosity)
    auto fIn  = make_unique<TFile>(input .c_str(), "READ"),
         fOut = make_unique<TFile>(output.c_str(), "RECREATE");
    JetEnergy::loopDirsFromGetResponse(fIn.get(), fOut.get());
}

} // end of DAS::JetEnergy namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        fs::path input, output;

        DT::Options options("Fit jet response in bins of pt and eta. Any 3D histograms "
                            "with axes following the same convention will be parsed in "
                            "the identical way.");
        options.input ("input" , &input , "input ROOT file (from `getJetResponse`)")
               .output("output", &output, "output ROOT file");

        const auto& config = options(argc, argv);
        const int steering = options.steering();

        DAS::JetEnergy::fitJetResponse(input, output, config, steering);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
#endif
