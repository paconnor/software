#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <functional>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"

#include <TTree.h>
#include <TFile.h>
#include <TString.h>
#include <TH1.h>
#include <TH3.h>

#include "common.h"
#include "Core/JEC/interface/matching.h"

#include <darwin.h>

using namespace std;
namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::JetEnergy {

////////////////////////////////////////////////////////////////////////////////
/// Create a TH3 histogram for the response with appropriate axes
TH3 * makeRespHist (TString name)
{
    static int nResBins = 200;
    static auto resBins = getBinning(nResBins, 0, 2);
    static const char * axistitles = ";p_{T}^{gen};|#eta|^{rec};#frac{p_{T}^{rec}}{p_{T}^{gen}}";

    // Filling in bins of ptgen results in better conditioned response distributions
    // Pure rec level weight is used
    auto h = new TH3D("Response" + name, axistitles,
                nPtJERCbins, pt_JERC_edges.data(), nAbsEtaBins, abseta_edges.data(), nResBins, resBins.data());
    h->SetDirectory(0);
    return h;
}

////////////////////////////////////////////////////////////////////////////////
/// Get JER differential resolution and balance.
void getJetResponse
        (const vector<fs::path>& inputs, //!< input ROOT files (n-tuples)
         const fs::path& output, //!< name of output ROOT file (histograms)
         const pt::ptree& config, //!< config file from `DT::Options`
         const int steering, //!< steering parameters from `DT::Options`
         const DT::Slice slice = {1,0} //!< slices for running
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    unique_ptr<TChain> tIn = DT::GetChain(inputs);
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = unique_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    auto year = metainfo.Get<int>("flags", "year");
    auto R = metainfo.Get<int>("flags", "R");

    if (!isMC) 
        BOOST_THROW_EXCEPTION( DE::BadInput("Only MC may be used as input.", metainfo) );

    Matching<RecJet, GenJet>::maxDR = R/10./2.;
    cout << "Radius for matching: " << Matching<RecJet, GenJet>::maxDR << endl;

    RecEvent * rEv = nullptr;
    PileUp * pileUp = nullptr;
    tIn->SetBranchAddress("recEvent", &rEv);
    tIn->SetBranchAddress("pileup", &pileUp);

    vector<RecJet> * recjets = nullptr;
    vector<GenJet> * genjets = nullptr;
    tIn->SetBranchAddress("recJets", &recjets);
    tIn->SetBranchAddress("genJets", &genjets);

    TH3 * incl = makeRespHist("");
    vector<TH3*> rhobins;
    rhobins.reserve(nRhoBins.at(year));
    for (int rhobin = 1; rhobin <= nRhoBins.at(year); ++rhobin)
        rhobins.push_back( makeRespHist( Form("_rhobin%d", rhobin) ) );

    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        // Matching candidates recjets
        Matching<RecJet, GenJet> matching(*recjets);
        auto recWgt = rEv->weights.front();
        
        int irho = 0;
        while (pileUp->rho > rho_edges.at(year).at(irho+1) && irho < nRhoBins.at(year)) ++irho;
        if (irho >= nRhoBins.at(year))
            BOOST_THROW_EXCEPTION( DE::AnomalousEvent(Form("irho = %d > nRhoBins.at(%d) = %d",
                                                        irho, year, nRhoBins.at(year)), tIn) );

        for (const auto& genjet: *genjets) {
            float ptgen = genjet.p4.Pt();
            assert(ptgen > 0);

            // Match: highest-pt approach is working much better
            //RecJet recjet = matching.Closest(genjet); //TODO: externalize option?
            RecJet recjet = matching.HighestPt(genjet);
            // TODO: JetMET matching?

            static const auto feps = numeric_limits<float>::max();
            if (recjet.p4.Pt() > feps/2.f) continue; // TODO??

            auto ptrec = recjet.CorrPt();

            // Fill resolution from smearing (here, *no* PS selection)
            auto resolution = ptrec/ptgen;
            auto etarec     = std::abs( recjet.p4.Eta() );
            // Inclusive rho case
            incl->Fill(ptgen, etarec, resolution, recWgt);

            // Exclusive rho case
            rhobins.at(irho)->Fill(ptgen, etarec, resolution, recWgt);
        } // End of for (gen jets) loop
    } // End of while (event) loop

    // Creating directory hierarchy and saving histograms
    incl->SetDirectory(fOut.get());
    incl->SetTitle("Response distribution (inclusive #rho)");
    incl->Write("Response");
    for (int irho = 0; irho < nRhoBins.at(year); ++irho) {
        TString title = Form("%.2f < #rho < %.2f", rho_edges.at(year).at(irho), rho_edges.at(year).at(irho+1));
        TDirectory * d_rho = fOut->mkdir(Form("rhobin%d", irho+1), title);
        d_rho->cd();
        rhobins.at(irho)->SetDirectory(fOut.get());
        rhobins.at(irho)->SetTitle("Response distribution (" + title + ")");
        rhobins.at(irho)->Write("Response");
    }

    metainfo.Set<bool>("git", "complete", true);
    tOut->Write();

    cout << __func__ << ' ' << slice << " end" << endl;
}

} // end of DAS::JetEnergy namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Get response in bins of pt and eta.", DT::split);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::JetEnergy::getJetResponse(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
