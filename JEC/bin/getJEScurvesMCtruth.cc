#include <cstdlib>
#include <string>
#include <iostream>
#include <filesystem>

#include <TFile.h>
#include <TROOT.h>
#include <TH2.h>
#include <TF1.h>
#include <TMath.h>

#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/variables.h"

#include "Core/JEC/interface/Scale.h"
#include "common.h"

#include <darwin.h>

using namespace std;
namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::JetEnergy {

void uncertainties (JetEnergy::Scale jecs)
{
    auto uncNames = JetEnergy::Scale::uncs;
    int N = uncNames.size();

    vector<TH2 *> uncertainties;
    for (auto uncName: uncNames) {
        // since up and down are symmetric, we only store "Up"
        TH2 * h = new TH2D(uncName, TString(uncName) + "_up", nPtJERCbins, pt_JERC_edges.data(), nEtaUncBins, eta_unc_edges.data());
        uncertainties.push_back(h);
    }

    cout << "Filling histograms with JES variation binning" << endl;

    for (int i = 0; i <= uncertainties.front()->GetNbinsX(); ++i)
    for (int j = 0; j <= uncertainties.front()->GetNbinsY(); ++j) {
        double pt  = uncertainties.front()->GetXaxis()->GetBinCenter(i),
               eta = uncertainties.front()->GetYaxis()->GetBinCenter(j);

        auto corrs = jecs.getUncVec(pt, eta);
        for (int k = 0; k < N; ++k) {
            double corr = corrs.at(2*k+1); // since up and down are symmetric, we only store "Up"
            uncertainties[k]->SetBinContent(i,j,corr);
        }
    }
    for (TH2 * h: uncertainties) 
        h->Write();
}

////////////////////////////////////////////////////////////////////////////////
/// Obtain JES corrections and uncertainties (L2 MCtruth)
void getJEScurvesMCtruth
            (const fs::path input, //!< directory to JetMET JES tables
             const fs::path output, //!< name of output root file containing the histograms
             const pt::ptree& config, //!< config file from `DT::Options`
             const int steering //!< steering parameters from `DT::Options`
            )
{
    unique_ptr<TFile> fOut(DT_GetOutput(output));

    cout << input.c_str() << endl;
    auto R = config.get<int>("flags.R");
    cout << R << endl;
    JetEnergy::Scale jes(input.c_str(), R);

    TH2 * h = new TH2D("MCtruth", "MCtruth", nPtJERCbins, pt_JERC_edges.data(), nAbsEtaBins, abseta_edges.data());

    for (int ibin = 1; ibin <= h->GetNbinsX(); ++ibin)
    for (int jbin = 1; jbin <= h->GetNbinsY(); ++jbin) {
        double  pt = h->GetXaxis()->GetBinCenter(ibin),
               eta = h->GetYaxis()->GetBinCenter(jbin);

        auto corr = jes.GetL2Relative(pt, eta);
        h->SetBinContent(ibin, jbin, corr);
    }
    h->Write();

    fOut->cd();

    cout << "Uncertainties" << endl;

    TDirectory * unc = fOut->mkdir("uncertainties");
    unc->cd();
    uncertainties(jes);
}

} // end of DAS namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        fs::path input, output;

        DT::Options options("Obtain JES corrections (L2Relative) from JetMET tables versus pt and eta");
        options.input   ("input" , &input   , "directory to tables provided by JetMET (in txt format)")
               .output  ("output", &output  , "output ROOT file")
               .arg<int>("R"     , "flags.R", "R parameter in jet clustering algorithm (x10)");

        const auto& config = options(argc, argv);
        const int steering = options.steering();

        DAS::JetEnergy::getJEScurvesMCtruth(input, output, config, steering);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
