# Jet Energy Corrections

[General TWiki](https://twiki.cern.ch/twiki/bin/view/CMS/JetMET)

Definition of levels in
[FactorizedJetCorrectorCalculator](https://github.com/cms-sw/cmssw/blob/master/CondFormats/JetMETObjects/src/FactorizedJetCorrectorCalculator.cc#L79).

## Jet Energy Scale with `applyJEScorrections`

Apply the JES corrections and produce a new *n*-tuple.
It uses `Scale.h`, implementing a class to read one JES tables.

## Jet Energy Resolution with `applyJERsmearing`

Apply the JER smearing and produce a new *n*-tuple.
It uses `Resolution.h`, implementing a class to read one JER tables.

[Dedicated TWiki](https://twiki.cern.ch/twiki/bin/viewauth/CMS/JetResolution#Smearing_procedures)

## Naming Conventions for JER inputs  

There is a suggested naming convention for the JER input files (e.g., .txt table files)  
that are meant to be archived on the JERCProtoLab repository.  

General template: `AnalysisName_year.txt`  
Some examples: `dijet_balance_UL18.txt`, `MCtruth_UL18.txt`, `RC_noise_UL18.txt`  

The files are meant to be pushed in the JERCProtoLab repository in the appropriate directories,  
according to the version of the correction they meant to be used (e.g., Summer20UL18)  
and in the respective appropriate subdirectory according to the purpose of the analysis that was  
contacted (e.g., `JER_SF`, `JER_inputs`, `MC_JER`).  

Link to the JERCProtoLab repository: [JERCProtoLab](https://gitlab.cern.ch/cms-jetmet/JERCProtoLab)
