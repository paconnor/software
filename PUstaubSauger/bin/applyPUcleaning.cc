#include <iostream>
#include <vector>
#include <filesystem>
#include <utility>
#include <unordered_map>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/ControlPlots.h"

#include "Core/PUstaubSauger/interface/Plots.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>

#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1.h>

#include <darwin.h>

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::PUstaub {

////////////////////////////////////////////////////////////////////////////////
/// Build a map from 2-column file with max gen pt and event IDs from MB sample
unordered_map<unsigned long long, float> GetMaxGenPts (const fs::path& input)
{
    unordered_map<unsigned long long, float> maxgenpts;
    if (input == "/dev/null")
        return maxgenpts;

    if (!fs::exists(input))
        BOOST_THROW_EXCEPTION(fs::filesystem_error("Missing 2-column file with max gen pt.",
                                input, make_error_code(errc::no_such_file_or_directory)));

    // use property tree to read the file
    pt::ptree prop_tree;
    read_info(input.c_str(), prop_tree);

    // but convert to unordered map for event loop
    // --> better optimised
    for (const auto& evtID_pt: prop_tree) {
        auto evtID = stoull(evtID_pt.first);
        auto pt    =        evtID_pt.second.get_value<float>();
        maxgenpts[evtID] = pt;
    }

    return maxgenpts;
}

////////////////////////////////////////////////////////////////////////////////
/// Remove PU staub (i.e. high-weight events due to PU sampling)
void applyPUcleaning
        (const vector<fs::path>& inputs, //!< input ROOT files (n-tuple)
         const fs::path& output, //!< output ROOT file (n-tuple)
         const pt::ptree& config, //!< config handled with `Darwin::Tools::Options`
         const int steering, //!< parameters obtained from explicit options
         const DT::Slice slice = {1,0} //!< number and index of slice
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;
    
    shared_ptr<TChain> tIn = DT::GetChain(inputs);
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = shared_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    if (!isMC) BOOST_THROW_EXCEPTION( DE::BadInput("Only MC may be used as input.",
                                      make_unique<TFile>(inputs.front().c_str() )) );
    auto R = metainfo.Get<int>("flags", "R");
    const auto maxDR = R/10.;

    auto fMBmaxgenpt = config.get<fs::path>("corrections.PUcleaning.MBmaxgenpt");
    const auto& maxgenpts = GetMaxGenPts(fMBmaxgenpt);
    metainfo.Set<fs::path>("corrections", "PUcleaning", "MBmaxgenpt", fMBmaxgenpt);

    // branches
    RecEvent * recEvt = nullptr;
    GenEvent * genEvt = nullptr;
    PileUp * pileup = nullptr;
    PrimaryVertex * pv = nullptr;
    tIn->SetBranchAddress("recEvent", &recEvt);
    tIn->SetBranchAddress("genEvent", &genEvt);
    tIn->SetBranchAddress("pileup", &pileup);
    tIn->SetBranchAddress("primaryvertex", &pv);
    vector<RecJet> * recjets = nullptr;
    vector<GenJet> * genjets = nullptr;
    tIn->SetBranchAddress("recJets", &recjets);
    tIn->SetBranchAddress("genJets", &genjets);

    auto pt_logw = make_unique<TH2F>("pt_logw", ";Jet p_{T}^{rec}   [GeV];log(w);N_{eff}^{j}", nPtBins, pt_edges.data(), 400, -30, 20),
         mjj_logw = make_unique<TH2F>("mjj_logw", ";m_{jj}^{rec}   [GeV];log(w);N_{eff}^{jj}", nMjjBins, Mjj_edges.data(), 400, -30, 20);

    ControlPlots::isMC = isMC;
    ControlPlots raw("raw"),
                 genveto("genveto"),
                 corrected("corrected");

    Plots PUbefore("PUbefore"),
          PUafter("PUafter"),
          PUgenveto("PUgenveto");

    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        raw(*genjets, genEvt->weights.front());
        raw(*recjets, genEvt->weights.front() * recEvt->weights.front());
        PUbefore(*genjets, *recjets, *genEvt, *recEvt, *pileup);

        // 1- removal based on the hard scale
        auto pthats_it = max_element(pileup->pthats.begin(), pileup->pthats.end());
        const float maxpthatPU = pthats_it != pileup->pthats.end() ? *pthats_it : 0.;
        bool bad_rec = (maxpthatPU > genEvt->hard_scale);

        // 2- removal based on the leading gen pt in the pileup
        if (!bad_rec)
        for (size_t i = 0; i < pileup->pthats.size(); ++i) {
            if (pileup->pthats.at(i) > 0) continue;
            unsigned long long evtID = pileup->MBevents.at(i);
            auto it = maxgenpts.find(evtID);
            if (it == maxgenpts.end()) continue;
            bad_rec |= it->second > genjets->front().p4.Pt();
            cout << recEvt->evtNo << ' ' << evtID << ' ' << it->second << endl;
            if (bad_rec) break;
        }

        // 3- removal of reconstruction artefacts happening at excessively high pt
        if (recjets->size() > 0) {
            const auto& recjet = recjets->begin();
            bool matched = false;
            for (const auto& genjet: *genjets)
                matched |= DeltaR(genjet.p4, recjet->p4) < maxDR;
            float maxgenpt = genjets->size() > 0 ? genjets->front().p4.Pt()
                                                 : genEvt->hard_scale;
            static const float m = 156; // TODO? currently tuned for Pythia UL18
            if (recjet->p4.Pt() > max(2*maxgenpt, m))
                recjets->erase(recjet);
        }

        // clean up collections
        if (bad_rec) {
            recjets->clear();
            recEvt->weights *= 0;
            genveto(*genjets, genEvt->weights.front());
            genveto(*recjets, genEvt->weights.front() * recEvt->weights.front());
            PUgenveto(*genjets, *recjets, *genEvt, *recEvt, *pileup);
        }
        pileup->pthats.clear();
        pileup->MBevents.clear();

        corrected(*genjets, genEvt->weights.front());
        corrected(*recjets, genEvt->weights.front() * recEvt->weights.front());
        PUafter(*genjets, *recjets, *genEvt, *recEvt, *pileup);

        if ((steering & DT::fill) == DT::fill) tOut->Fill();
    }

    metainfo.Set<bool>("git", "complete", true);
    fOut->cd();
    tOut->Write();
    raw.Write(fOut.get());
    genveto.Write(fOut.get());
    corrected.Write(fOut.get());
    PUbefore.Write(fOut.get());
    PUgenveto.Write(fOut.get());
    PUafter.Write(fOut.get());

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of DAS::PUstaub namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Clean up events from anomalous pile-up in MC samples.", DT::config | DT::split | DT::fill);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<fs::path>("MBmaxgenpt", "corrections.PUcleaning.MBmaxgenpt",
                                "2-column file with MB event ID and max gen pt");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::PUstaub::applyPUcleaning(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
